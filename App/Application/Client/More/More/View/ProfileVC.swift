//
//  ProfileVC.swift
//  App
//
//  Created by Abdo Emad on 30/06/2024.
//

import UIKit

class ProfileVC: BaseViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: NormalTextFieldView!
    @IBOutlet weak var userPhone: PhoneTextFieldView!
    @IBOutlet weak var userEmail: EmailTextFieldView!
    
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var imageData : Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getProfile()
    }

    private func setUp(){
        userImage.layer.cornerRadius = userImage.frame.height / 2
        addBackButtonWith(title: "Personal_Profile_More_Title".localized)
    }
    
    @IBAction func pickImageButtonTapped(_ sender: UIButton) {
        ImagePicker().pickImage { image, imageData in
            self.userImage.image = image
            self.imageData = imageData
        }
    }
    
  
    @IBAction func ChagnePhoneNumbButtonTapped(_ sender: UIButton) {
        let vc = ActivationCodeVC.sendToActivationCodeVC(phone: userPhone.phoneTextValue()!, countryCode: userPhone.countryCodeLabel.text!, vcType: .profileVC)
        push(vc)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        do{
            let name = try ValidationService.validate(name: userName.textValue())
            let email = try userEmail.emailText()
            updatePatientProfile(image: imageData, email: email, name: name)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    
}

//MARK: - NewtWorking

extension ProfileVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func getProfile(){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.getProfile()
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                userImage.setWith(response.data?.avatar)
                userName.set(text: response.data?.name)
                userPhone.set(text: response.data?.phone)
                userEmail.set(text: response.data?.email)
                userPhone.countryCodeLabel.text = response.data?.countryCode
                userPhone.flagImage.setWith(response.data?.countryFlag)
            }catch{
                
            }
        }
    }
    private func updatePatientProfile(image:Data?, email: String, name: String ){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.updatePatientProfile(image: image, email: email, name: name)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    UserDefaults.userData?.name = response.data?.name ?? ""
                    UserDefaults.userData?.avatar = response.data?.avatar ?? ""
                    UserDefaults.userData?.email = response.data?.email ?? ""
                    show(successMessage: response.message)
                    pop()
                }
            }catch{
                print("error trying to update profile : \(error.localizedDescription)")
            }
        }
    }
}
