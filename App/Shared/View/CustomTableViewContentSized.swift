//
//  OffersTableViewContentSized.swift
//  azzam-client
//
//  Created by Yosef elbosaty on 11/09/2023.
//

import UIKit

class OffersTableViewContentSized: UITableView {
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height < 480 ? contentSize.height : 480)
    }
}


class RateTableViewContentSized: UITableView {
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height < 640 ? contentSize.height : 640)
    }
}
