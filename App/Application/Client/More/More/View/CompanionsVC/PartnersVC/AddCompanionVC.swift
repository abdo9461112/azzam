//
//  AddNewPartnerVC.swift
//  App
//
//  Created by Abdo Emad on 03/07/2024.
//

import UIKit

class AddCompanionVC: BaseViewController{
    
    
    @IBOutlet weak var newUserLable: UILabel!
    @IBOutlet weak var currentUserLable: UILabel!
    
    @IBOutlet weak var newButt: UIButton!
    @IBOutlet weak var currentButt: UIButton!
    @IBOutlet weak var userName: NormalTextFieldView!
    @IBOutlet weak var userphone: PhoneTextFieldView!
    @IBOutlet weak var userEmail: EmailTextFieldView!
    @IBOutlet weak var userCity: DropDownTextFieldView!
    @IBOutlet weak var useRelations: DropDownTextFieldView!
    
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var kinshipsModel: [GlobalData] = []
    private var countriesModel : [Countries] = []
    private var citiesModel : [City] = []
    private var cityId : String = " "
    private var realtiveId : String = " "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getGolble()
    }
    
    private func setUp(){
        newButt.isSelected = true
        currentButt.isSelected = false
        currentUserLable.textColor = .lightGray
        newButt.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        addBackButtonWith(title: "Companions".localized)
        userCity.delegate = self
        useRelations.delegate = self
        userphone.addTapGesture {
            self.showCountryCodePicker()
        }
    }
    private func setInitialCountryCode(_ country: Countries) {
        userphone.countryCodeLabel.text = country.code
        userphone.flagImage.setWith(country.image)
        getCities(countryId: country.id)
    }
    @IBAction func newUserButtonTapped(_ sender: UIButton) {
        sender.isSelected = true
        currentButt.isSelected = false
        [userName, userEmail,userCity].forEach { view in
            view?.isHidden = false
        }
        newUserLable.textColor = .black
        currentUserLable.textColor = .lightGray
        newButt.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        currentButt.setImage(UIImage(systemName: "circle"), for: .normal)
    }
    
    @IBAction func currentUserButtonTapped(_ sender: UIButton) {
        sender.isSelected = true
        newButt.isSelected = false
        [userName, userEmail,userCity].forEach { view in
            view?.isHidden = true
        }
        newUserLable.textColor = .lightGray
        currentUserLable.textColor = .black
        currentButt.setImage(UIImage(systemName: "circle.fill"), for: .normal)
        newButt.setImage(UIImage(systemName: "circle"), for: .normal)
    }
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        if newButt.isSelected{
            newUserValidation()
        }else{
            currentUserValidation()
        }
    }
    
    private func newUserValidation(){
        do{
            let name = try ValidationService.validate(name: userName.textValue())
            let phone = try userphone.phoneText()
            let email = try userEmail.emailText()
            let cityId = try ValidationService.validate(cityId: cityId)
            let countryCode = try userphone.countryCodeText()
            let realtiveId = try ValidationService.validate(kinship: realtiveId)
            addCompanionNewUser(name: name, phone: phone, email: email, city: cityId, countryCode: countryCode, kinship: realtiveId)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    private func currentUserValidation(){
        do{
            let phone = try userphone.phoneText()
            let countryCode = try userphone.countryCodeText()
            let realtiveId = try ValidationService.validate(kinship: realtiveId)
            addCompanionExsitUser(phone: phone, countryCode: countryCode, kinship: realtiveId)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    
}

//MARK: - DropDown

extension AddCompanionVC: CountryCodeDelegate, DropDownTextFieldViewDelegate {
    
    func didSelectCountry(_ item: Countries) {
        userphone.countryCodeLabel.text = item.code
        userphone.flagImage.setWith(item.image)
        getCities(countryId: item.id)
    }
    func dropDownList(for textFieldView: DropDownTextFieldView) -> [any DropDownItem] {
        if textFieldView == userCity{
            return citiesModel
        }else if textFieldView == useRelations{
            return kinshipsModel
        }
        return kinshipsModel
    }
    
    func didSelect(item: any DropDownItem, for textFieldView: DropDownTextFieldView) {
        if textFieldView == userCity{
            cityId = item.id
        }else if textFieldView == useRelations{
            realtiveId = item.id
        }
    }
    
}

//MARK: - NetWorking
extension AddCompanionVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func addCompanionNewUser(name:String,phone:String,email:String,city:String,countryCode:String,kinship:String){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.addCompanionNewUser(name: name, phone: phone, email: email, city: city, userType: "patient", countryCode: countryCode, kinship: kinship)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    pop()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                
            }
        }
    }
    private func addCompanionExsitUser(phone: String,countryCode:String,kinship:String){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.addCompanionExsitUser(phone: phone, countryCode: countryCode, kinship: kinship)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    pop()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                
            }
        }
    }
    private func getGolble(){
        Task{
            let endpoint = SettingsEndpoints.getGlobal()
            do{
                guard let response = try await request(endpoint)else{return}
                kinshipsModel = response.data?.kinships ?? []
                let countries = response.data?.countries ?? []
                userphone.set(countryCodes: countries)
                if let firstCountry = countries.first {
                    setInitialCountryCode(firstCountry)
                }
            }catch{
            }
        }
    }
    private func getCities(countryId: String) {
        Task {
            let endpoint = SettingsEndpoints.getCities(countryId: countryId)
            do {
                guard let response = try await request(endpoint) else { return }
                citiesModel = response.data ?? []
            } catch {
                print("error trying to call get Cities API: \(error.localizedDescription)")
            }
        }
    }
    private func showCountryCodePicker() {
        Task {
            let endpoint = SettingsEndpoints.getCountries()
            do {
                guard let response = try await request(endpoint) else { return }
                let vc = CountriesVC(countries: response.data ?? [], delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            } catch {
                print("error trying to call get Countries API: \(error.localizedDescription)")
            }
        }
    }
}


