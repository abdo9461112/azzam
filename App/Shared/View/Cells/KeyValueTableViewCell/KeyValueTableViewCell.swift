//
//  KeyValueTableViewCell.swift
//  App
//
//  Created by MGAbouarab on 11/01/2024.
//

import UIKit

class KeyValueTableViewCell: UITableViewCell {
    
    struct Configuration {
        let font: UIFont
        let keyColor: UIColor
        let valueColor: UIColor
        let backgroundColor: UIColor
        
        static let `default` = Configuration(
            font: .appRegular(size: 14),
            keyColor: .secondaryLabel,
            valueColor: .secondaryLabel,
            backgroundColor: .systemBackground
        )
        
        static let secondary = Configuration(
            font: .appBold(size: 16),
            keyColor: .main,
            valueColor: .main,
            backgroundColor: .main
        )
        
    }
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var keyLabel: UILabel!
    @IBOutlet weak private var valueLabel: UILabel!
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configure()
        self.selectionStyle = .none
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.configure()
    }
    
    //MARK: - Design -
    func configure(_ configuration: Configuration = .default) {
        self.keyLabel.font = configuration.font
        self.valueLabel.font = configuration.font
        self.keyLabel.textColor = configuration.keyColor
        self.valueLabel.textColor = configuration.valueColor
        self.containerView.backgroundColor = configuration.backgroundColor
    }
    
    //MARK: - Data -
    func set(key: String, value: String) {
        self.keyLabel.text = key
        self.valueLabel.text = value
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
