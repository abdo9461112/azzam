//
//  AuthEndpoints.swift
//  App
//
//  Created by MGAbouarab on 22/12/2023.
//

import Foundation

struct AuthEndpoints {
    private init() {}
}

struct EmptyResponse :Decodable {}

extension AuthEndpoints {
    
    static
    func login(phone: String, countryCode: String) -> Endpoint<BaseResponse<UserModel>> {
        return .init(
            method: .post,
            path: "signin",
            body: [
                "loginKey": phone,
                "countryCode": countryCode,
                "deviceId": UserDefaults.pushNotificationToken ?? "no device id for firebase for this device and" ,
                "deviceType": "ios",
                "userType":"patient"
            ]
        )
    }
    
    static
    func signUp(name:String,phone: String, countryCode: String,email:String,cityId:String) -> Endpoint<BaseResponse<UserModel>> {
        return .init(
            method: .post,
            path: "signup-patient",
            body: [
                "phone": phone,
                "name":name,
                "email":email,
                "countryCode": countryCode,
                "city":cityId,
                "userType":"patient"
            ]
        )
    }
    static
    func activatetionCode(phone: String,code:String, countryCode: String) -> Endpoint<BaseResponse<UserModel>> {
        return .init(
            method: .post,
            path: "activate",
            body: [
                "loginKey": phone,
                "countryCode": countryCode,
                "deviceId": UserDefaults.pushNotificationToken ?? "no device id for firebase for this device and" ,
                "deviceType": "ios",
                "userType":"patient",
                "activationCode":code
            ]
        )
    }
    
    static
    func resendCode(phone: String, countryCode: String) -> Endpoint<BaseResponse<EmptyResponse>> {
        return .init(
            method: .patch,
            path: "resend-code",
            body: [
                "loginKey": phone,
                "countryCode": countryCode,
                "userType":"patient"
            ]
        )
    }
    
    static
    func sendCode(phone: String,countryCode: String) -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .patch,
                    path: "send-code",
                    body:
                        [
                            "phone":phone,
                            "countryCode":countryCode,
                            "userType":"patient"

                        ]
        )
    }
    
    static
    func changePhoneNumber(newPhone: String,newCountryCode: String) -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .patch,
                    path: "send-code",
                    body:
                        [
                            "updatedPhone":newPhone,
                            "updatedCountryCode":newCountryCode,
                            "userType":"patient"
                        ]
        )
    }
    
    static
    func activateNewPhoneNumber(phone: String,countryCode: String, code:String) -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .patch,
                    path: "update-phone-number",
                    body:
                        [
                            "phone":phone,
                            "countryCode":countryCode,
                            "code":code
                        ]
        )
    }
    
}

