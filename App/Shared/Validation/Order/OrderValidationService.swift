//
//  OrderValidationService.swift
//  App
//
//  Created by MGAbouarab on 11/01/2024.
//

import Foundation

struct OrderValidationService {
    
    static func validate(orderDetails: String?) throws -> String {
        guard let orderDetails = orderDetails?.trimWhiteSpace(), !orderDetails.isEmpty else {
            throw OrderValidationError.emptyOrderDetails
        }
        return orderDetails
    }
    
    static func validate(paymentMethod: PaymentType?) throws -> String {
        guard let key = paymentMethod?.key.trimWhiteSpace(), !key.isEmpty else {
            throw OrderValidationError.emptyOrderPaymentType
        }
        return key
    }
  
    static func validate(duration: Int?) throws -> Int {
        guard let duration = duration else {
            throw OrderValidationError.emptyOrderDurationTime
        }
        return duration
    }
    
}
