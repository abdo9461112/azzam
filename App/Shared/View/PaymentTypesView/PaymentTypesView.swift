//
//  PaymentTypesView.swift
//  App
//
//  Created by MGAbouarab on 09/01/2024.
//

import UIKit

class PaymentTypesView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var tableView: UITableView!
    
    //MARK: - Properties -
    private var types: [PaymentType] = []
    var selectedType: PaymentType? {
        self.types.first(where: {$0.isSelected})
    }
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PaymentTypesView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.layer.cornerRadius = .cardCorner
        self.clipsToBounds = true
        self.setupTableView()
    }
    
    //MARK: - Data -
    func set(types: [PaymentType]) {
        defer {
            self.tableView.reloadData()
        }
        self.types = types
        guard !self.types.isEmpty else {return}
        guard self.types.allSatisfy({$0.isSelected == false}) else {return}
        self.types[0].isSelected = true
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}

//MARK: - TableView -
extension PaymentTypesView {
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(cellType: PaymentTypeTableViewCell.self)
    }
}
extension PaymentTypesView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.types.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: PaymentTypeTableViewCell.self, for: indexPath)
        cell.set(self.types[indexPath.row])
        return cell
    }
}
extension PaymentTypesView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let index = self.types.firstIndex(where: {$0.isSelected}) {
            self.types[index].isSelected = false
            self.tableView.reloadRows(at: [.init(row: index, section: 0)], with: .fade)
        }
        self.types[indexPath.row].isSelected = true
        self.tableView.reloadRows(at: [indexPath], with: .fade)
    }
}
