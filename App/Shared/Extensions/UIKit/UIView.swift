//
//  UIView.swift
//  App
//
//  Created by MGAbouarab on 27/12/2023.
//

import UIKit

//MARK: - Animation -
extension UIView {
    
    @objc func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.2
        pulse.fromValue = 1.0
        pulse.toValue = 0.98
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: nil)
    }
    @objc func flash() {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = 0.3
        flash.fromValue = 1
        flash.toValue = 0.6
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = 1
        layer.add(flash, forKey: nil)
    }
    
    @objc func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-10.0, 10.0, -10.0, 10.0, -5.0, 5.0, -2.5, 2.5, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get{
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

}

extension UIView {
    func addActiveBorder() {
        self.layer.borderColor = .mainCGColor
        self.layer.borderWidth = 1
    }
    func addInactiveBorder() {
        self.layer.borderColor = UIColor.separator.cgColor
        self.layer.borderWidth = 1
    }
    func removeBorder() {
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 1
    }
    
    func addDashedBorder(withColor color: UIColor = .main) {
        let color = color.cgColor
        let name = "dashedLayer"
        for layer in self.layer.sublayers ?? [] {
            if layer.name == name {
                layer.removeFromSuperlayer()
            }
        }
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        shapeLayer.name = name
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: self.layer.cornerRadius).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}

extension UIView {
    func dropShadow(color: UIColor = .label.withAlphaComponent(0.1)) {
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = .init(width: 0, height: -3)
        layer.shadowRadius = 4
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}

extension UIView {
    private func viewContainingController() -> UIViewController? {

        var nextResponder: UIResponder? = self

        repeat {
            nextResponder = nextResponder?.next

            if let viewController = nextResponder as? UIViewController {
                return viewController
            }

        } while nextResponder != nil

        return nil
    }

    func parentViewController() -> UIViewController? {

        var matchController = viewContainingController()
        var parentContainerViewController: UIViewController?

        if var navController = matchController?.navigationController {

            while let parentNav = navController.navigationController {
                navController = parentNav
            }

            var parentController: UIViewController = navController

            while let parent = parentController.parent,
                (parent.isKind(of: UINavigationController.self) == false &&
                    parent.isKind(of: UITabBarController.self) == false &&
                    parent.isKind(of: UISplitViewController.self) == false) {

                        parentController = parent
            }

            if navController == parentController {
                parentContainerViewController = navController.topViewController
            } else {
                parentContainerViewController = parentController
            }
        } else if let tabController = matchController?.tabBarController {

            if let navController = tabController.selectedViewController as? UINavigationController {
                parentContainerViewController = navController.topViewController
            } else {
                parentContainerViewController = tabController.selectedViewController
            }
        } else {
            while let parentController = matchController?.parent,
                (parentController.isKind(of: UINavigationController.self) == false &&
                    parentController.isKind(of: UITabBarController.self) == false &&
                    parentController.isKind(of: UISplitViewController.self) == false) {

                        matchController = parentController
            }

            parentContainerViewController = matchController
        }

        let finalController = parentContainerViewController

        return finalController
    }
    var parentContainerViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while let responder = parentResponder {
            if let viewController = responder as? UIViewController {
                return viewController
            }
            parentResponder = responder.next
        }
        return nil
    }
}

private struct AssociatedKeys {
    static var tapHandler = UnsafeRawPointer(bitPattern: 0)
    static var swipeHandler = UnsafeRawPointer(bitPattern: 0)
}

//MARK: - Tap -
extension UIView {
    func addTapGesture(handler: @escaping () -> Void) {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGesture)
        
        // Store the closure in an associated object
        objc_setAssociatedObject(self, &AssociatedKeys.tapHandler, handler, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    @objc private func handleTap(_ gesture: UITapGestureRecognizer) {
        if let tapHandler = objc_getAssociatedObject(self, &AssociatedKeys.tapHandler) as? () -> Void {
            tapHandler()
        }
    }
}

//MARK: - Swipe -
extension UIView {
    func addSwipeGesture(handler: @escaping () -> Void) {
        let swipeGesture = UIPanGestureRecognizer(target: self, action: #selector(handleSwipeGesture(_:)))
        self.addGestureRecognizer(swipeGesture)
        objc_setAssociatedObject(self, &AssociatedKeys.swipeHandler, handler, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    @objc private func handleSwipeGesture(_ gesture: UIPanGestureRecognizer) {
        
        
        if let swipeHandler = objc_getAssociatedObject(self, &AssociatedKeys.swipeHandler) as? () -> Void {
            let translation = gesture.translation(in: self.superview)
            let velocity = gesture.velocity(in: self.superview)
            
            switch gesture.state {
            case .began, .changed:
                // Update the view's position based on the gesture translation
                if translation.y > 0 {
                    self.transform = CGAffineTransform(translationX: 0, y: translation.y)
                } else if translation.y < 0 && translation.y > -30 {
                    let x = -(translation.y / 100)
                    self.transform = CGAffineTransform(scaleX: 1, y: 1+x)
                }
                
                
            case .ended:
                // Calculate the dismissal threshold based on the view's height
                let dismissalThreshold = self.bounds.height * 0.3
                
                // Dismiss the view if swiped downward or with sufficient velocity
                if translation.y > dismissalThreshold || velocity.y > 1000 {
                    swipeHandler()
                } else {
                    // Return the view to its original position
                    UIView.animate(withDuration: 0.3) {
                        self.transform = .identity
                    }
                }
                
            default:
                break
            }
        }
        
    }
}
