//
//  WalletVC.swift
//  App
//
//  Created by Abdo Emad on 29/06/2024.
//

import UIKit

class WalletVC: BaseViewController {
    @IBOutlet weak var balanceLable: UILabel!
    
    private let responseHandler:ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBalance()
        setUp()
    }
    private func setUp(){
        addBackButtonWith(title: "Wallet".localized)
    }

    @IBAction func chargeWalletButtonTapped(_ sender: UIButton) {
    }
    
}

//MARK: - NetWorking

extension WalletVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
         return try await self.responseHandler.get(endPoint, progress: progress)
     }
    private func getBalance(){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.getWallet()
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                balanceLable.text = response.data
            }catch{
                
            }
        }
    }
}
