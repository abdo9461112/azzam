//
//  ConnectUsVC.swift
//  App
//
//  Created by Abdo Emad on 29/06/2024.
//

import UIKit

class ConnectUsVC: BaseViewController {
    @IBOutlet weak var userName: NormalTextFieldView!
    @IBOutlet weak var userPhone: PhoneTextFieldView!
    @IBOutlet weak var userTitle: NormalTextFieldView!
    @IBOutlet weak var userMessage: AppTextView!
    
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        fetchCountryCodes()
    }
    
    private func setUp(){
        addBackButtonWith(title: "Contact_Us_More_Title".localized)
        userPhone.addTapGesture {
            self.showCountryCodePicker()
        }
    }
    
    private func setInitialCountryCode(_ country: Countries) {
        userPhone.countryCodeLabel.text = country.code
        userPhone.flagImage.setWith(country.image)
    }
    
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        do{
            let name = try ValidationService.validate(name: userName.textValue())
            let phone = try userPhone.phoneText()
            let title = try ValidationService.validate(title: userTitle.textValue())
            let message = try ValidationService.validate(message: userMessage.textValue())
            contactus(phone: phone, name: name, countryCode: userPhone.countryCodeLabel.text!, title: title, message: message)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    
}

//MARK: - NetWorking

extension ConnectUsVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
         return try await self.responseHandler.get(endPoint, progress: progress)
     }
    
    private func contactus(phone: String, name: String, countryCode:String, title: String, message:String){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.contactus(phone: phone, name: name, countryCode: countryCode, title: title, message: message, type: "patient")
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    pop()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                
            }
        }
    }
    private func fetchCountryCodes() {
        Task {
            let endpoint = SettingsEndpoints.getCountries()
            do {
                guard let response = try await request(endpoint) else { return }
                let countries = response.data ?? []
                userPhone.set(countryCodes: countries)
                if let firstCountry = countries.first {
                    setInitialCountryCode(firstCountry)
                }
            } catch {
                print("error trying to call get Countries API: \(error.localizedDescription)")
            }
        }
    }
    
    private func showCountryCodePicker() {
        Task {
            let endpoint = SettingsEndpoints.getCountries()
            do {
                guard let response = try await request(endpoint) else { return }
                let vc = CountriesVC(countries: response.data ?? [], delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            } catch {
                print("error trying to call get Countries API: \(error.localizedDescription)")
            }
        }
    }

}



extension ConnectUsVC: CountryCodeDelegate {
    func didSelectCountry(_ item: Countries) {
        userPhone.countryCodeLabel.text = item.code
        userPhone.flagImage.setWith(item.image)
    }
}
