//
//  HomeDataTypes.swift
//  App
//
//  Created by MGAbouarab on 02/01/2024.
//

import Foundation

struct HomeDataModel: Codable {
    let title, key: String
    let list: [HomeData]
}

struct HomeData: Codable {
    let id, image, icon, avatar, message, name, link, color, slug, doctorName, centerName, description, department: String?
//    let credentials: LiveStreamData?
    let joinButton: Bool?
}

struct BroadcastModel: Codable {
    let id, doctorName, avatar, centerName, description, name, message: String
//    let credentials: LiveStreamData
    let joinButton: Bool
}
//
//struct LiveStreamData: Codable {
//    let apiKey, sessionId, token: String
//}
//
//struct BroadcastCommentModel: Codable {
//    let id, name, avatar, comment, createdAt: String
//}


//struct HomeData: Codable {
//    let key: String
//    let list: [List]
//    let rotation: String?
//    let title: String
//    let value: Bool
//}
//
//struct List: Codable {
//    let id: String?
//    let image: String?
//    let color: String?
//    let department: String?
//    let icon: String?
//    let name: String?
//    let slug: String?
//    
//}
