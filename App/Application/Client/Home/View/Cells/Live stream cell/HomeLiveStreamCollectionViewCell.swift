//
//  HomeLiveStreamCollectionViewCell.swift
//  App
//
//  Created by Abdo Emad on 15/07/2024.
//

import UIKit

class HomeLiveStreamCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var doctorAvatarImageView: UIImageView!
    @IBOutlet weak var broadcastTitleLabel: UILabel!
    @IBOutlet weak var doctorNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        doctorAvatarImageView.image = nil
    }

    func configureCell(liveStream: HomeData) {
        doctorAvatarImageView.setWith(liveStream.avatar)
        broadcastTitleLabel.text = liveStream.name
        doctorNameLabel.text = liveStream.doctorName
    }
}
