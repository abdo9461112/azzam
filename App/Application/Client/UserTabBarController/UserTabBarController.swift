//
//  UserTabBarController.swift
//  App
//
//  Created by MGAbouarab on 30/12/2023.
//

import UIKit

class UserTabBarController: UITabBarController {
    
    
    //MARK: - Properties -
    
    //MARK: - LifeCycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.initialView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard UserDefaults.isFirstTime else {return}
//        UserDefaults.isFirstTime = false
//        let vc = UIViewController()
//        vc.view.backgroundColor = .secondarySystemBackground
//        let nav = BaseNavigationController(root: vc)
//        nav.modalPresentationStyle = .fullScreen
//        self.present(nav, animated: false)
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        /// To Animate tabBar item
        guard let barItemView = item.value(forKey: "view") as? UIView else { return }

        let timeInterval: TimeInterval = 0.4
        let propertyAnimator = UIViewPropertyAnimator(duration: timeInterval, dampingRatio: 0.5) {
            barItemView.transform = CGAffineTransform.identity.scaledBy(x: 0.8, y: 0.8)
        }
        propertyAnimator.addAnimations({ barItemView.transform = .identity }, delayFactor: CGFloat(timeInterval))
        propertyAnimator.startAnimation()
        
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        
    }
    open override func setViewControllers(_ viewControllers: [UIViewController]?, animated: Bool) {
        super.setViewControllers(viewControllers, animated: animated)
    }
    
    
    //MARK: - Design -
    private func initialView(){
        self.setupDesign()
        self.addChilds()
    }
    private func addChilds() {
        self.viewControllers = [
            home(),
            consultings(),
            categories(),
            notifcations(),
            account()
        ]
    }
    private func setupDesign() {
        
        
        let selectedAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.main,
            NSAttributedString.Key.font: UIFont.appBold(size: 10)
        ]
        let normalAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.secondaryLabel,
            NSAttributedString.Key.font: UIFont.appBold(size: 11)
        ]
        let badgeAttribute: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font : UIFont.appBold(size: 10)
        ]
        
        let tabBarAppearance = UITabBarAppearance()
        let tabBarItemAppearance = UITabBarItemAppearance(style: .inline)
        tabBarItemAppearance.normal.titleTextAttributes = normalAttributes
        tabBarItemAppearance.selected.titleTextAttributes = selectedAttributes
        
        tabBarAppearance.stackedLayoutAppearance = tabBarItemAppearance
        
        self.tabBar.tintColor = .main
        self.tabBarItem.setBadgeTextAttributes(badgeAttribute, for: .normal)
        self.tabBarItem.setBadgeTextAttributes(badgeAttribute, for: .selected)
        
        self.tabBar.standardAppearance = tabBarAppearance
        
    }

    //MARK: - Tabbar VCs -
    func home() -> UINavigationController {
        let vc = HomeViewController()
        vc.tabBarItem = .init(title: "Home_page_title_tabBar".localized, image: UIImage(systemName: "house.fill"), tag: 0)
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
       return ColoredNav(rootViewController: vc)

    }
    func consultings() -> UINavigationController {
        let vc = UIViewController()
        vc.view.backgroundColor = .secondarySystemBackground
        vc.tabBarItem = .init(title: "Consultings".localized, image: UIImage(resource: .consultations), selectedImage: UIImage(resource: .activeConsultations))
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        return ColoredNav(rootViewController: vc)
    }
    func categories() -> UINavigationController {
        let vc = UIViewController()
        vc.view.backgroundColor = .secondarySystemBackground
        vc.tabBarItem = .init(title: "Categories".localized, image: UIImage(resource: .departments), selectedImage: .activeDepartments)
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        return ColoredNav(rootViewController: vc)
    }
    func notifcations() -> UINavigationController {
        let vc = UIViewController()
        vc.view.backgroundColor = .secondarySystemBackground
        vc.tabBarItem = .init(title: "Notifications".localized, image: UIImage(resource: .notifications), selectedImage: .notificationsActive)
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        return ColoredNav(rootViewController: vc)
    }
    func account() -> UINavigationController {
        let vc = MoreViewController()
        vc.tabBarItem = .init(title: "More_title_tabBar".localized, image: UIImage(systemName: "square.grid.2x2"), tag: 2)
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -3, right: 0)
        return ColoredNav(rootViewController: vc)
    }
    
    //MARK: - Logic -
    
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
extension UserTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let _ = viewControllers else { return false }
        guard let fromView = selectedViewController?.view, let toView = viewController.view else {
            return false
        }
        
        guard fromView != toView else {
            return false
        }
        
        UIView.transition(from: fromView, to: toView, duration: 0.3, options: [.transitionCrossDissolve], completion: nil)
        return true
    }
}


