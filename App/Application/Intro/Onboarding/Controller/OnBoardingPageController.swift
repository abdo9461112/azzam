//
//  OnBoardingPageController.swift
//  App
//
//  Created by MGAbouarab on 28/12/2023.
//

import UIKit

class OnBoardingPageController: UIPageViewController {
    
    //MARK: - Properties -
    private let subViewControllers: [UIViewController]
    private let items: [OnboardingModel]
    private var isLast = false
    private lazy var myView: OnBoardingPageView = OnBoardingPageView()
    private var forwardDirection: UIPageViewController.NavigationDirection {
        Language.isRTL() ? .reverse : .forward
    }
    private var reverseDirection: UIPageViewController.NavigationDirection {
        !Language.isRTL() ? .reverse : .forward
    }
    
    //MARK: - Initializer -
    init(items: [OnboardingModel]) {
        var viewControllers: [UIViewController] = []
        for item in items {
            viewControllers
                .append(
                    OnboardingViewController(
                        image: item.image
                    )
                )
        }
        self.subViewControllers = viewControllers
        self.items = items
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal)
    }
    required init?(coder: NSCoder) {
        self.subViewControllers = []
        self.items = []
        super.init(coder: coder)
    }
    
    //MARK:  - Lifecycle -
    override func loadView() {
        super.loadView()
        self.view.addSubview(myView)
        self.view.bringSubviewToFront(myView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myView.setPages(count: self.subViewControllers.count)
        self.myView.onNext = { [weak self] in
            guard let self = self else {return}
            self.isLast ? self.skip() : self.goToNextPage()
        }
        self.myView.onSkip = { [weak self] in
            self?.skip()
        }
        self.setupPageController()
        self.addMyViewConstraints()
        
    }
    
    //MARK: - Design -
    private func addMyViewConstraints() {
        self.myView.translatesAutoresizingMaskIntoConstraints = false
        self.myView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        self.myView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        self.myView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    }
    
    //MARK: - Logic -
    private func handleButtons(forIndex currentIndex: Int) {
        if currentIndex == subViewControllers.count - 1 {
            isLast = true
            self.myView.setSkipButton(alpha: 0)
            self.myView.setNextButton(title: "Start_now_button".onboardingLocalized)
        } else if currentIndex == subViewControllers.count - 2 && isLast {
            isLast = false
            self.myView.setSkipButton(alpha: 1)
            self.myView.setNextButton(title: "Next_intro_button".onboardingLocalized)
        }
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}

//MARK: - PageViewController-
private extension OnBoardingPageController {
    private func setupPageController() {
        self.dataSource = self
        guard let first = self.subViewControllers.first else {return}
        guard let currentItem = self.items.first else {return}
        self.setViewControllers([first], direction: forwardDirection, animated: true, completion: nil)
        self.myView.set(title: currentItem.title, description: currentItem.description, pageIndex: 0)
    }
    private func goToNextPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        let currentIndex = self.subViewControllers.firstIndex(of: nextViewController) ?? 0
        self.handleButtons(forIndex: currentIndex)
        let currentItem = self.items[currentIndex]
        self.myView.set(title: currentItem.title, description: currentItem.description, pageIndex: currentIndex)
        setViewControllers([nextViewController], direction: forwardDirection, animated: animated, completion: nil)
        
    }
    private func goToPreviousPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let previousViewController = dataSource?.pageViewController(self, viewControllerBefore: currentViewController) else { return }
        let currentIndex = self.subViewControllers.firstIndex(of: previousViewController) ?? 0
        self.handleButtons(forIndex: currentIndex)
        let currentItem = self.items[currentIndex]
        self.myView.set(title: currentItem.title, description: currentItem.description, pageIndex: currentIndex)
        setViewControllers([previousViewController], direction: reverseDirection, animated: animated, completion: nil)
    }
}
extension OnBoardingPageController: UIPageViewControllerDataSource {
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.subViewControllers.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = self.subViewControllers.firstIndex(of: viewController) else {return nil}
        self.handleButtons(forIndex: currentIndex)
        let currentItem = self.items[currentIndex]
        self.myView.set(title: currentItem.title, description: currentItem.description, pageIndex: currentIndex)
        guard currentIndex > 0 else { return nil }
        return subViewControllers[currentIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentIndex = subViewControllers.firstIndex(of: viewController) else {return nil}
        self.handleButtons(forIndex: currentIndex)
        let currentItem = self.items[currentIndex]
        self.myView.set(title: currentItem.title, description: currentItem.description, pageIndex: currentIndex)
        guard currentIndex < self.subViewControllers.count - 1 else { return nil}
        return self.subViewControllers[currentIndex + 1]
    }
    
}

//MARK: - Route -
extension OnBoardingPageController {
    func skip() {
        let vc = LoginVC()
        let navController = UINavigationController(rootViewController: vc)
        AppHelper.changeWindowRoot(vc: navController)
    }
}
