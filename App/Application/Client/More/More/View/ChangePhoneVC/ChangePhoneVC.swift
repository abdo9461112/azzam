//
//  ChangePhoneVC.swift
//  App
//
//  Created by Abdo Emad on 30/06/2024.
//

import UIKit

class ChangePhoneVC: BaseViewController {
    @IBOutlet weak var newPhone: PhoneTextFieldView!
    
    let responseHandler : ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

    private func setUp(){
        addBackButtonWith(title: "Change phone number".localized)
        setInitial()
        newPhone.addTapGesture {
            self.showCountryCodePicker()
        }
    }
    
    private func setInitial() {
        newPhone.countryCodeLabel.text = UserDefaults.userData?.countryCode
        newPhone.set(text: UserDefaults.userData?.phone)
        newPhone.flagImage.setWith(UserDefaults.userData?.countryFlag)
    }
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        do{
            let phone = try newPhone.phoneText()
            let countryCode = try newPhone.countryCodeText()
            changePhoneNumber(newPhone: phone, newCountryCode: countryCode)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    

}

extension ChangePhoneVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func changePhoneNumber(newPhone: String, newCountryCode: String){
        Task{
            showIndicator()
            let endpoint = AuthEndpoints.changePhoneNumber(newPhone: newPhone, newCountryCode: newCountryCode)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    let vc = ActivationCodeVC.sendToActivationCodeVC(phone: newPhone, countryCode: newCountryCode, vcType: .activateNewNumber)
                    push(vc)
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                
            }
        }
    }
    private func showCountryCodePicker() {
        Task {
            let endpoint = SettingsEndpoints.getCountries()
            do {
                guard let response = try await request(endpoint) else { return }
                let vc = CountriesVC(countries: response.data ?? [], delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            } catch {
                print("error trying to call get Countries API: \(error.localizedDescription)")
            }
        }
    }
}

extension ChangePhoneVC: CountryCodeDelegate {
    func didSelectCountry(_ item: Countries) {
        newPhone.countryCodeLabel.text = item.code
        newPhone.flagImage.setWith(item.image)
    }
}
