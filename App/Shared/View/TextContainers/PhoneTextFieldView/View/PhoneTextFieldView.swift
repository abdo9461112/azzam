//
//  PhoneTextFieldView.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import UIKit

final class PhoneTextFieldView: TextFieldView {
    
    //MARK: - IBOutlet -
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak  var textField: UITextField!
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var countryCodeView: UIView!
    @IBOutlet weak  var countryCodeLabel: UILabel!
    @IBOutlet weak private var imageView: UIImageView!
    @IBOutlet weak var flagImage: UIImageView!
    
    @IBOutlet weak var stakView: UIStackView!
    
    //MARK: - Properties -
    @IBInspectable var image: UIImage? {
        didSet {
            self.imageView.image = image?.withRenderingMode(.alwaysTemplate)
        }
    }
    @IBInspectable var enableCode: Bool = true {
        didSet {
            self.countryCodeView.isHidden = !enableCode
        }
    }
    private let pickerView = UIPickerView()
    private var countryCodes: [Countries] = []

    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PhoneTextFieldView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.setupTextField()
        self.setupLocalization()
        self.setupContainerView()
        self.getLocalCountries()
    }
//    private func addSelectCountryCodeFeature() {
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showCountryCode))
//        self.countryCodeView.addGestureRecognizer(tap)
//    }
    private func getLocalCountries() {
        if let data = self.readLocalJsonFile(forName: Language.isRTL() ? "countriesAr" : "countriesEn") {
            if let countries: [Countries] = self.parse(jsonData: data) {
                self.countryCodes = countries
            }
        }
    }
    private func setupContainerView() {
        self.containerView.layer.cornerRadius = containerViewCornerRadius
        self.containerView.clipsToBounds = true
        self.setInactiveState()
    }
    private func setupTextField() {
        self.textField.delegate = self
    }
    private func setupLocalization() {
        self.titleLabel.text = "Phone Number".phoneNumberLocalizable
        self.textField.placeholder = "Please enter your phone number".phoneNumberLocalizable
    }
    private func setActiveState() {
        UIView.animate(withDuration: 0.2) {
            self.containerView.addActiveBorder()
            self.tintColor = self.activeTintColor
        }
    }
    private func setInactiveState() {
        UIView.animate(withDuration: 0.2) {
            self.containerView.removeBorder()
            self.tintColor = self.inActiveTintColor
        }
    }
    
    
    //MARK: - Load resources -
    private func readLocalJsonFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
    private func parse<T: Decodable>(jsonData: Data) -> T? {
        do {
            let decodedData = try JSONDecoder().decode(T.self, from: jsonData)
            return decodedData
        } catch {
            return nil
        }
    }
    
    
    //MARK: - Encapsulation -
    func set(text: String?) {
        guard let text, !text.trimWhiteSpace().isEmpty else {
            self.textField.text = nil
            self.setInactiveState()
            return
        }
        self.textField.text = text
        self.setActiveState()
    }
    func set(countryCodes: [Countries]) {
        self.countryCodes = countryCodes
    }
//    private func set(countryCode: CountryCodeModel) {
//        self.selectedCountryCode = countryCode
//    }
//    func set(code: String?) {
//        guard let countryCode = self.countryCodes.filter({$0.countryCode == code}).first else {
//            self.selectedCountryCode = self.countryCodes.first
//            return
//        }
//        self.selectedCountryCode = countryCode
//    }
    func phoneText() throws -> String {
        do {
            return try PhoneValidationService.validate(phone: self.textField.text)
        } catch {
//            self.shake()
            throw error
        }
    }
    func phoneTextValue() -> String? {
        self.textField.text
    }
    func countryCodeText() throws -> String {
        do {
            return try PhoneValidationService.validate(countryCode: self.countryCodeLabel.text)
        } catch {
            throw error
        }
    }
    
    //MARK: - Actions -
    func showCountryCode() {
        let vc = CountriesVC(countries: self.countryCodes, delegate: self)
        let nav = BaseNavigationController(rootViewController: vc)
        self.parentContainerViewController?.present(nav, animated: true)
        
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
extension PhoneTextFieldView: CountryCodeDelegate {
    func didSelectCountry(_ item: Countries) {
        countryCodeLabel.text = item.name
        flagImage.setWith(item.image)
    }
}
extension PhoneTextFieldView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.setActiveState()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, !text.trimWhiteSpace().isEmpty else {
            self.setInactiveState()
            return
        }
        self.setActiveState()
    }
}
