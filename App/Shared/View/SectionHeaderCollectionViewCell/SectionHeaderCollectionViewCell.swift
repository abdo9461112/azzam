//
//  SectionHeaderCollectionViewCell.swift
//  App
//
//  Created by MGAbouarab on 26/01/2024.
//

import UIKit

class SectionHeaderCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var titleLabel: UILabel!
    
    //MARK: - Data -
    func set(title: String, font: UIFont = .appSemiBold(size: 14)) {
        self.titleLabel.text = title.capitalized
        self.titleLabel.font = font
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}
