//
//  HomeCenterVC.swift
//  App
//
//  Created by Abdo Emad on 17/07/2024.
//

import UIKit

class HomeCenterVC: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var name, id , slug : String?
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var groupsModel : [CenterGroupsData] = []{
        didSet{
            tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getConsultationGroups()
    }

    override func viewWillAppear(_ animated: Bool) {
        (self.navigationController as? ColoredNav)?.changeApperance(to: .light)
    }
    
    private func setUp(){
        addBackButtonWith(title: name ?? "Back".localized)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(cellType: HomeCenterTableViewCell.self)
    }
    static func create(title: String, id : String , slug: String) -> HomeCenterVC{
        let vc = HomeCenterVC()
        vc.id = id
        vc.name = title
        vc.slug = slug
        return vc
    }
}

//MARK: -  TableView
extension HomeCenterVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        groupsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: HomeCenterTableViewCell.self, for: indexPath)
        cell.set(groupsModel[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    
    
}
//MARK: - NewtWorking
extension HomeCenterVC{
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func getConsultationGroups(){
        Task{
            showIndicator()
            let endpoint = HomeEndPoints.getConsultationGroups(id: id ?? "", name: name ?? "")
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                groupsModel = response.data?.consultationGroups ?? [ ]
            }catch{
                print("error trying to call get Consultation Groups api request \(error.localizedDescription)")
            }
        }
    }
    
}






