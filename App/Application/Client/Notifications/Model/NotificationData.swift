//
//  NotificationData.swift
//  App
//
//  Created by MGAbouarab on 07/01/2024.
//

import Foundation

struct NotificationData: Decodable {
    
    let id: String
    let icon: String
    let title: String
    let message: String
    let type: String
    let orderId: Int
    let orderOwner : Int
    let notificationType: NotificationType
    let createdAt: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case icon
        case title
        case message
        case type
        case orderId = "order_id"
        case orderOwner = "order_owner"
        case notificationType = "notification_type"
        case createdAt = "created_at"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(String.self, forKey: .id) ?? ""
        self.icon = try container.decodeIfPresent(String.self, forKey: .icon) ?? ""
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
        self.type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
        self.orderId = try container.decodeIfPresent(Int.self, forKey: .orderId) ?? 0
        self.orderOwner = try container.decodeIfPresent(Int.self, forKey: .orderOwner) ?? 0
        self.notificationType = try container.decodeIfPresent(NotificationType.self, forKey: .notificationType) ?? .adminNotify
        self.createdAt = try container.decodeIfPresent(String.self, forKey: .createdAt) ?? ""
    }
    
}
