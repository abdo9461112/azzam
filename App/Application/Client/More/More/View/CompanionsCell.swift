//
//  CompanionsCell.swift
//  App
//
//  Created by Abdo Emad on 03/07/2024.
//

import UIKit

class CompanionsCell: UITableViewCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var kinshipLable: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var onDelete : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setData( _ data : CompanionsModel){
        userName.text = data.name
        kinshipLable.text = data.kinship
        userImage.setWith(data.avatar)
    }
   
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        onDelete?()
    }
    
}
