import UIKit

class LoginVC: BaseViewController {
    
    @IBOutlet weak var userPhone: PhoneTextFieldView!
    
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        fetchCountryCodes()
    }
    
    private func setUp() {
        setLeading(title: "Login".localized)
        userPhone.addTapGesture {
            self.showCountryCodePicker()
        }
    }
    
    private func setInitialCountryCode(_ country: Countries) {
        userPhone.countryCodeLabel.text = country.code
        userPhone.flagImage.setWith(country.image)
    }
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        do {
            let phone = try userPhone.phoneText()
            let countryCode = try userPhone.countryCodeText()
            login(phone: phone, countryCode: countryCode)
        } catch {
            show(errorMessage: error.localizedDescription)
        }
    }
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        let vc = SignUpVC()
        push(vc)
    }
    
    @IBAction func visitorButtonTapped(_ sender: UIButton) {
        UserDefaults.isFirstTime = true
        UserDefaults.isLogin = false
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
    
    
}

//MARK: - Networking

extension LoginVC {
    
   private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func login(phone: String, countryCode: String) {
        Task {
            showIndicator()
            let endpoint = AuthEndpoints.login(phone: phone, countryCode: countryCode)
            do {
                guard let response = try await request(endpoint) else { return }
                hideIndicator()
                if response.key == .needActive {
                    show(successMessage: response.message)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1 ) {
                        let vc = ActivationCodeVC.sendToActivationCodeVC(phone: phone, countryCode: countryCode, vcType: .loginVC)
                        let nav = UINavigationController(rootViewController: vc)
                        AppHelper.changeWindowRoot(vc: nav)
                    }
                } else {
                    show(errorMessage: response.message)
                }
            } catch {
                print("error calling login request API: \(error.localizedDescription)")
            }
        }
    }
    private func fetchCountryCodes() {
        Task {
            let endpoint = SettingsEndpoints.getCountries()
            do {
                guard let response = try await request(endpoint) else { return }
                let countries = response.data ?? []
                userPhone.set(countryCodes: countries)
                if let firstCountry = countries.first {
                    setInitialCountryCode(firstCountry)
                }
            } catch {
                print("error trying to call get Countries API: \(error.localizedDescription)")
            }
        }
    }
    
    private func showCountryCodePicker() {
        Task {
            let endpoint = SettingsEndpoints.getCountries()
            do {
                guard let response = try await request(endpoint) else { return }
                let vc = CountriesVC(countries: response.data ?? [], delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            } catch {
                print("error trying to call get Countries API: \(error.localizedDescription)")
            }
        }
    }
    
}

extension LoginVC: CountryCodeDelegate {
    func didSelectCountry(_ item: Countries) {
        userPhone.countryCodeLabel.text = item.code
        userPhone.flagImage.setWith(item.image)
    }
}
