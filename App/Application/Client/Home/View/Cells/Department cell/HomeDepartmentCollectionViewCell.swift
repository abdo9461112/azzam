//
//  HomeDepartmentCollectionViewCell.swift
//  App
//
//  Created by Abdo Emad on 15/07/2024.
//

import UIKit

class HomeDepartmentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var departmentIcon: UIImageView!
    @IBOutlet weak var departmentTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(department: HomeData) {
        departmentIcon.setWith(department.image)
        departmentTitleLabel.text = department.name
    }

}
