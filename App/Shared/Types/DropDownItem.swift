//
//  DropDownItem
//  App
//
//  Created by MGAbouarab on 10/01/2024.
//

import Foundation

protocol DropDownItem {
    var id: String { get }
    var name: String { get }
}
