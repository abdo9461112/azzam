//
//  SceneDelegate.swift
//  App
//
//  Created by MGAbouarab on 20/12/2023.
//

import UIKit
import IQKeyboardManagerSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    static private(set) var current: SceneDelegate?
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        SceneDelegate.current = self
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        switch UserDefaults.isFirstTime {
        case true:
            let vc = IntroSelectLanguageViewController()
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        case false:
            let vc = UserTabBarController()
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }
    }
    func sceneWillEnterForeground(_ scene: UIScene) {
        
    }
    
}
