//
//  CGColor.swift
//  App
//
//  Created by MGAbouarab on 27/12/2023.
//

import UIKit

extension CGColor {
    
    static var mainCGColor: CGColor {
        return UIColor.main.cgColor
    }
//    static var mainWithAlpha: CGColor {
//        let x = UIColor.mainWithAlpha
//        return x.cgColor
//    }
//    
//    static var secondary: CGColor {
//        let x = UIColor.secondary
//        return x.cgColor
//    }
//    static var secondaryWithAlpha: CGColor {
//        let x = UIColor.secondaryWithAlpha
//        return x.cgColor
//    }
    
}

extension UIColor {
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        let hexString: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
