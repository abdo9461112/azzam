//
//  MoreModel.swift
//  App
//
//  Created by Abdo Emad on 30/06/2024.
//

import Foundation

struct MedicalRecordsModel: Codable {
    let id, orderNumber, startedAt, endedAt: String
}

struct CommonQuestionsModel: Codable {
    let id, question, answer: String
    var isSelected: Bool? = false

}

struct PartnersModel: Codable {
    let id, name, image: String
}

struct PartnerDetailsModel: Codable {
    let id, image, name, description: String
}

struct CompanionsModel : Codable{
    let id, name, avatar, kinship: String

}

struct PaymentMethod : Codable{
    let id, slug: String
}
