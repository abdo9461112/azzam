//
//  SuccessVC.swift
//  App
//
//  Created by Abdo Emad on 04/07/2024.
//

import UIKit

class SuccessVC: BaseViewController {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var forwardButton: UIButton!
    
    private var titleText: String?
    private var titleButtonText: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLable.text = titleText
        forwardButton.setTitle(titleButtonText, for: .normal)
    }

    static func create(titleLable:String,titleButton:String) -> SuccessVC{
        let vc = SuccessVC()
        vc.titleText = titleLable
        vc.titleButtonText = titleButton
        return vc
    }
    @IBAction func moveForwardButtonTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func goToHomeButtonTapped(_ sender: UIButton) {
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
    
}
