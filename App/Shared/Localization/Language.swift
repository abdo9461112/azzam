//
//  Language.swift
//  App
//
//  Created by MGAbouarab on 29/12/2023.
//

import UIKit

struct Language {
    
    enum Languages {
        static let en = "en"
        static let ar = "ar"
    }
    
    static func currentLanguage() -> String {
        let languages = UserDefaults.standard.object(forKey: "AppleLanguages") as! NSArray
        let firstLanguage = languages.firstObject as! String
        return firstLanguage
    }
    static func setAppLanguage(lang: String) {
        guard !self.currentLanguage().lowercased().contains(lang.lowercased()) else {return}
        UserDefaults.standard.set([lang, currentLanguage()], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        Language.handleViewDirection()
        Bundle.swizzleLocalization()
    }
    
    static func apiLanguage() -> String {
        return self.currentLanguage().contains(Languages.ar) ? Languages.ar : Languages.en
    }
    static func isRTL() -> Bool {
        return self.currentLanguage().contains(Languages.ar) ? true : false
    }
    
    static func handleViewDirection() {
        UIPageControl.appearance().semanticContentAttribute = isRTL() ? .forceRightToLeft : .forceLeftToRight
        UIStackView.appearance().semanticContentAttribute = isRTL() ? .forceRightToLeft : .forceLeftToRight
        UISwitch.appearance().semanticContentAttribute = isRTL() ? .forceRightToLeft : .forceLeftToRight
        UIView.appearance().semanticContentAttribute = isRTL() ? .forceRightToLeft : .forceLeftToRight
        UICollectionView.appearance().semanticContentAttribute = isRTL() ? .forceRightToLeft : .forceLeftToRight
        UITextField.appearance().textAlignment = isRTL() ? .right : .left
        UILabel.appearance().textAlignment = isRTL() ? .right : .left
        UITextView.appearance().semanticContentAttribute = isRTL() ? .forceRightToLeft : .forceLeftToRight
        UITextField.appearance().semanticContentAttribute = isRTL() ? .forceRightToLeft : .forceLeftToRight
    }
    
}
extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    var validationLocalized: String {
        return NSLocalizedString(self, comment: "")
    }
    
}
