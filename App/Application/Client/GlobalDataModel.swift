import Foundation

// MARK: - DataClass
struct GlobalDataModel: Codable {
    let countries: [Countries]
    let kinships: [GlobalData]
    let centers: [GlobalData]
    let specializations: [GlobalData]
    let addiction: [GlobalData]
    let jobs: [GlobalData]
    let center: GlobalData
    let terms: String
}

struct GlobalData: Codable, DropDownItem {
    let id, name, image, code: String
}

struct Countries: Codable {
    var id: String
    var code: String?
    var name: String
    var image: String?
}
struct City: Codable, DropDownItem {
    var id: String
    var name: String
}

