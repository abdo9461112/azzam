//
//  NotificationsViewModel.swift
//  App
//
//  Created by MGAbouarab on 07/01/2024.
//

import Foundation

final class NotificationsViewModel: BaseViewModel {
    
    //MARK: - Properties -
    @Published private(set) var notifications: [NotificationData] = []
    private var isLastPage: Bool = false
    private var isFetching: Bool = false
    private var currentPage: Int = 1
    
    //MARK: - Actions -
    func loadMore() {
        self.getNotifications()
    }
    func startGettingNotifications() {
        self.notifications = []
        self.isLastPage = false
        self.isFetching = false
        self.currentPage = 1
        self.getNotifications()
    }
    
}

//MARK: - Networking -
extension NotificationsViewModel {
    private func getNotifications() {
//        guard !isFetching, !isLastPage else {return}
//        Task {
//            self.isFetching = true
//            let endpoint = HomeEndPoints.notifications(
//                page: self.currentPage
//            )
//            do {
//                guard let response = try await self.request(endpoint) else {return}
//                self.notifications += response.notifications
//                self.isLastPage = response.pagination.isLastPage
//                self.currentPage += 1
//            } catch {
//                print(error.localizedDescription)
//            }
//            self.isFetching = false
//        }
    }
}
