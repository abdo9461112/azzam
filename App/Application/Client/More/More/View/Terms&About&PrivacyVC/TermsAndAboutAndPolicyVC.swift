//
//  TermsAndAboutAndPrivacyVC.swift
//  App
//
//  Created by Abdo Emad on 29/06/2024.
//

import UIKit
import SnapKit
enum TermsTypes {
    
    case terms
    case about
    case policy
}

class TermsAndAboutAndPolicyVC: BaseViewController {
    private var termsTypes : TermsTypes = .terms
    private let responseHandler: ResponseHandler = DefaultResponseHandler()
    
    private var termsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        return view
    }()
    
    private let imageView : UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = UIImage(resource: .logo)
        return image
    }()
    private var textView : UITextView = {
        let text = UITextView()
        text.backgroundColor = .clear
        text.isEditable = false
        text.textColor = .black
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    private lazy var stackView : UIStackView = {
        let stack = UIStackView(
            arrangedSubviews:
                [
                    imageView,
                    textView
                ]
        )
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = 10
        stack.axis = .vertical
        return stack
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        layOut()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch termsTypes {
        case .terms:
            getTerms()
            addBackButtonWith(title: "Terms and condtions".localized)
        case .about:
            getAbout()
            addBackButtonWith(title: "About_Us_More_Title".localized)
        case .policy:
            getPolicy()
            addBackButtonWith(title: "Privacy_More_Title".localized)
        }
    }
    
    static func create(type: TermsTypes) -> TermsAndAboutAndPolicyVC{
        let vc = TermsAndAboutAndPolicyVC()
        vc.termsTypes = type
        return vc
    }
    
    private func setViews(){
        view.backgroundColor = .systemGray6
        view.addSubview(termsView)
        termsView.addSubview(stackView)
    }
    
    private func layOut(){
        termsView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(100)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
            make.bottom.equalToSuperview().offset(-50)
        }
        stackView.snp.makeConstraints { make in
            make.leading.equalTo(termsView.snp.leading).offset(16)
            make.trailing.equalTo(termsView.snp.trailing).offset(-16)
            make.top.bottom.equalToSuperview()
        }
        imageView.snp.makeConstraints { make in
            make.height.equalTo(200)
        }
        
    }
    
}
//MARK: - NetWroking

extension TermsAndAboutAndPolicyVC {
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    func getTerms() {
        Task {
            showIndicator()
            do {
                if let htmlString = try await request(SettingsEndpoints.getTerms())?.data,
                   let htmlData = htmlString.data(using: .utf8),
                   let attributedString = try? NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                    hideIndicator()
                    textView.attributedText = attributedString
                    textView.font = UIFont.systemFont(ofSize: 17)
                } else {
                    print("Error: Unable to process HTML string")
                }
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    func getAbout() {
        Task {
            showIndicator()
            do {
                if let htmlString = try await request(SettingsEndpoints.getAbout())?.data,
                   let htmlData = htmlString.data(using: .utf8),
                   let attributedString = try? NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                    hideIndicator()
                    textView.attributedText = attributedString
                    textView.font = UIFont.systemFont(ofSize: 17)
                } else {
                    print("Error: Unable to process HTML string")
                }
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    } 
    func getPolicy() {
        Task {
            showIndicator()
            do {
                if let htmlString = try await request(SettingsEndpoints.getPolicy())?.data,
                   let htmlData = htmlString.data(using: .utf8),
                   let attributedString = try? NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                    hideIndicator()
                    textView.attributedText = attributedString
                    textView.font = UIFont.systemFont(ofSize: 17)
                } else {
                    print("Error: Unable to process HTML string")
                }
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}
