//
//  BaseResponse.swift
//  App
//
//  Created by MGAbouarab on 22/12/2023.
//

import Foundation

struct BaseResponse<T: Decodable>: Decodable {
    var key: ServerResponseKey
    var message: String
    var data: T?
    var paginate: Pagination?

    
    enum CodingKeys: String, CodingKey {
        case key
        case message 
        case data
    }
    
    struct Pagination: Codable {
        let currentPage: Int
        let lastPage: Int
    }
}
