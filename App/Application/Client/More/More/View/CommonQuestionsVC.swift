//
//  CommanQuestionsVC.swift
//  App
//
//  Created by Abdo Emad on 29/06/2024.
//

import UIKit

class CommonQuestionsVC: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    private var commonQuestionsModel : [CommonQuestionsModel] = []
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getCommonQuestions()
    }
    
    private func setUp(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: CommonQuestionsCell.self)
        addBackButtonWith(title: "Common questions".localized)
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
    }

}

//MARK: - Tableview

extension CommonQuestionsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commonQuestionsModel.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: CommonQuestionsCell.self, for: indexPath)
        cell.setData(model: commonQuestionsModel[indexPath.row])
        cell.selectionStyle = .none
        cell.onCliking = {[weak self] in
            guard let self = self else {return}
            self.commonQuestionsModel[indexPath.row].isSelected = (self.commonQuestionsModel[indexPath.row].isSelected == true) ? false : true
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }
        return cell
    }
  
    
}

//MARK: - Networking

extension CommonQuestionsVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
         return try await self.responseHandler.get(endPoint, progress: progress)
     }
    
    private func getCommonQuestions(){
        showIndicator()
        Task{
            let endpoint = SettingsEndpoints.getCommonQuestions()
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                commonQuestionsModel = response.data  ?? []
                for index in self.commonQuestionsModel.indices {
                    self.commonQuestionsModel[index].isSelected = false
                }
                tableView.reloadData()
            }catch{
                print("error trying call common questions : \(error.localizedDescription)")
            }
        }
    }
}





