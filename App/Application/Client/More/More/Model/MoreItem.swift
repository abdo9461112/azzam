//
//  MoreItem.swift
//  App
//
//  Created by MGAbouarab on 16/01/2024.
//

import UIKit

struct MoreItem {
    let image: UIImage?
    let name: String?
    var accessoryType: UITableViewCell.AccessoryType = .disclosureIndicator
    var backgroundColor: UIColor = .systemBackground
    let action: (()->Void)
}
