//
//  ProductEndPoints.swift
//  App
//
//  Created by MGAbouarab on 26/01/2024.
//

import Foundation

struct ProductEndPoints {
    private init() {}
}

extension ProductEndPoints {
    static
    func details(id: Int) -> Endpoint<BaseResponse<EmptyResponse>> {
        return .init(
            path: "single-product",
            queries: [
                "product_id": id
            ]
        )
    }
    
    static
    func selectGroup(forProductId id: Int, andSelectedOptions options: [Int]) -> Endpoint<BaseResponse<EmptyResponse>> {
        return .init(
            path: "select-group",
            queries: [
                "product_id": id,
                "properities": "\(options)"
            ]
        )
    }
}
 


