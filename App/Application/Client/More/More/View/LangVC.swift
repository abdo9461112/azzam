//
//  LangVC.swift
//  App
//
//  Created by Abdo Emad on 10/07/2024.
//

import UIKit

class LangVC: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private let padding: CGFloat = 16
    private let spacing: CGFloat = 10
    var selectedLang = "en"
    var languages: [IntroSelectLanguage] = [
        .init(
            image: "arabicFlag",
            displayedName: "اللغة العربية",
            localeSymbol: Language.Languages.ar,
            isSelected: Language.isRTL()
        ),
        .init(
            image: "englishFlag",
            displayedName: "English",
            localeSymbol: Language.Languages.en,
            isSelected: !Language.isRTL()
        )
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        self.addBackButtonWith(title: "App Language".localized)
        if Language.currentLanguage() == "ar" {
            
            self.languages[0].isSelected = true
        }else{
            self.languages[1].isSelected = true

        }
    }


    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        Language.handleViewDirection()
        Language.setAppLanguage(lang: selectedLang)
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
    

}
//MARK: - collectionView

extension LangVC {
    func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(cellType: IntroSelectLanguageCollectionViewCell.self)
        self.collectionView.contentInset = .init(top: 0, left: padding, bottom: 0, right: padding)
    }
}

extension LangVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.languages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: IntroSelectLanguageCollectionViewCell.self, for: indexPath)
        cell.set(language: languages[indexPath.row])
        return cell
    }
}

extension LangVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedIndex = self.languages.firstIndex(where: {$0.isSelected}) {
            self.languages[selectedIndex].isSelected = false
            collectionView.reloadItems(at: [IndexPath(item: selectedIndex, section: 0)])
            self.languages[indexPath.row].isSelected = true
            self.selectedLang = self.languages[indexPath.row].localeSymbol
            
            collectionView.reloadItems(at: [indexPath])
        }
    }
}

extension LangVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let languageCount: CGFloat = 2//CGFloat(languages.count)
        let allSpacing = spacing*languageCount-1
        let length = (collectionView.bounds.width - 2*padding - allSpacing)/languageCount
        return .init(
            width: length,
            height: length
        )
    }
}
