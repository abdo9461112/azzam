//
//  SettingsVC.swift
//  App
//
//  Created by Abdo Emad on 29/06/2024.
//

import UIKit

class SettingsVC: BaseViewController {
    
    @IBOutlet weak var notifcationsView: UIView!
    @IBOutlet weak var langView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        addBackButtonWith(title: "Settings".localized)
        notifcationsView.addTapGesture {
            self.goToNotifcationsVC()
        }
        langView.addTapGesture {
            self.goToLangVC()
        }
    }
    
    private func goToNotifcationsVC(){
       let vc = NotifcationsVC()
        push(vc)
    }
    private func goToLangVC(){
       let vc = LangVC()
        push(vc)
    }
    
}


 


