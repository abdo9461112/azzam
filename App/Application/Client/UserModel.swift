//
//  Model.swift
//  App
//
//  Created by Abdo Emad on 27/06/2024.
//


// MARK: - DataClass
struct UserModel: Codable {
    var id, token, name, email, phone, countryCode, avatar, cityName, cityId, userType,countryFlag: String
    var isNotify: Bool
}

