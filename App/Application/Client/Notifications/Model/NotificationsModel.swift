//
//  NotificationsModel.swift
//  App
//
//  Created by MGAbouarab on 07/01/2024.
//

import Foundation

struct NotificationsModel: Decodable {
    
    let notifications: [NotificationData]
    let pagination: PaginationModel
    
    enum CodingKeys: CodingKey {
        case notifications
        case pagination
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.notifications = try container.decodeIfPresent([NotificationData].self, forKey: .notifications) ?? []
        self.pagination = try container.decodeIfPresent(PaginationModel.self, forKey: .pagination) ?? .init()
    }
}
