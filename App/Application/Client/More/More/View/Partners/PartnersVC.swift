//
//  PartnersVC.swift
//  App
//
//  Created by Abdo Emad on 03/07/2024.
//

import UIKit
import SnapKit

class PartnersVC: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var partnersModel : [PartnersModel] = [] {
        didSet{
            collectionView.reloadData()
            if partnersModel.isEmpty{
                emptyMessage.isHidden = false
            }else{
                emptyMessage.isHidden = true
            }
        }
    }
    
    
    private let emptyMessage : UILabel = {
        let lable = UILabel()
        lable.text = "Empty data".localized
        lable.font = UIFont.systemFont(ofSize: 18)
        return lable
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getPartners()
    }

    private func setUp(){
        addBackButtonWith(title: "Partners".localized)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellType: PartnersCell.self)
        view.addSubview(emptyMessage)
        emptyMessage.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
    }

}

//MARK: - CollectionView
extension PartnersVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        partnersModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: PartnersCell.self, for: indexPath)
        let item = partnersModel[indexPath.row]
        cell.setData(item)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = partnersModel[indexPath.row]
        let vc = PartnersDetailsVC.create(id: item.id)
        push(vc)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width - 24) / 2, height: 200)
    }
}

//MARK: - netWorking
extension PartnersVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
         return try await self.responseHandler.get(endPoint, progress: progress)
     }
    
    private func getPartners(){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.getPartners()
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                partnersModel = response.data ?? []
            }catch{
                
            }
        }
    }
}





