//
//  CenterDetailsVC.swift
//  App
//
//  Created by Abdo Emad on 17/07/2024.
//

import UIKit

class CenterDetailsVC: BaseViewController {

    private var  id , slug, name : String?
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func setUp(){
        addBackButtonWith(title: name ?? "Back".localized)
    }
    static func create(id: String, slug: String,name: String) -> CenterDetailsVC{
        let vc = CenterDetailsVC()
        vc.id = id
        vc.slug = slug
        return vc
    }
}

//extension 
