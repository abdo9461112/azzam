//
//  IntroSelectLanguageViewDelegate.swift
//  App
//
//  Created by MGAbouarab on 26/12/2023.
//

import Foundation

protocol IntroSelectLanguageViewDelegate: AnyObject {
    var languages: [IntroSelectLanguage] {get}
    func didSelect(languageAtIndex index: Int)
}
