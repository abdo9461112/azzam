//
//  TermsVC.swift
//  App
//
//  Created by Abdo Emad on 29/06/2024.
//

import UIKit
import SnapKit

class TermsVC: BaseViewController{
    
    private var imageView : UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFit
        image.image = UIImage(resource: .logo)
        return image
    }()
    
    private var textView : UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isEditable = false
        textView.font = UIFont.systemFont(ofSize: 20)
        textView.textColor = .black
        textView.backgroundColor = .clear
        return textView
    }()
    
    private lazy var VstackView : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [
            imageView,
            textView
        ])
        stack.axis = .vertical
        stack.spacing = 20
        stack.alignment = .fill
        stack.distribution = .fill
        return stack
    }()
    
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layOut()
        getTermsData()
    }
    
    private func layOut(){
        addBackButtonWith(title: "Terms and Conditions".localized)
        view.addSubview(VstackView)
        view.backgroundColor = .systemBackground
        VstackView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(100)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
            make.bottom.equalToSuperview().offset(-20)
        }
        imageView.snp.makeConstraints { make in
            make.height.equalTo(200)
        }
    }
}

// MARK: - Networking

extension TermsVC {
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    func getTermsData() {
        Task {
            showIndicator()
            do {
                if let htmlString = try await request(SettingsEndpoints.getTerms())?.data,
                   let htmlData = htmlString.data(using: .utf8),
                   let attributedString = try? NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) {
                    hideIndicator()
                    textView.attributedText = attributedString
                } else {
                    print("Error: Unable to process HTML string")
                }
            } catch {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}
