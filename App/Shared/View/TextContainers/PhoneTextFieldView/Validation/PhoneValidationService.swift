//
//  PhoneValidationService.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation

struct PhoneValidationService {
    static func validate(phone: String?) throws -> String {
        guard let phone = phone, !phone.trimWhiteSpace().isEmpty else {
            throw PhoneValidationErrors.emptyPhone
        }
//        guard phone.isValidPhoneNumber(pattern: #"^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$"#) else {
//            throw PhoneValidationErrors.inValidPhone
//        }
        return phone
    }
    static func validate(countryCode: String?) throws -> String {
        guard let countryCode = countryCode, !countryCode.trimWhiteSpace().isEmpty else {
            throw PhoneValidationErrors.emptyCountryCode
        }
        return countryCode
    }
}

