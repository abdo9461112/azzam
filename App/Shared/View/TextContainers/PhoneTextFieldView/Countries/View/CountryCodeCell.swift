//
//  CountryCodeCell.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import UIKit
import SnapKit

class CountryCodeCell: UITableViewCell {
    
    private var flagImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var nameLabel: UILabel = {
        let label = UILabel()
        label.text = nil
        label.textColor = .label
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var codeLabel: UILabel = {
        let label = UILabel()
        label.text = nil
        label.numberOfLines = 2
        label.sizeToFit()
        label.textColor = .label
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupInitialView()
        self.resetData()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupInitialView()
        self.resetData()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.resetData()
    }
    
    private func setupInitialView() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        let leadingStack = UIStackView(arrangedSubviews: [flagImageView, nameLabel])
        leadingStack.alignment = .center
        leadingStack.distribution = .fill
        leadingStack.spacing = 8
        leadingStack.axis = .horizontal
        let stack = UIStackView(arrangedSubviews: [leadingStack, codeLabel])
        stack.alignment = .fill
        stack.distribution = .equalSpacing
        stack.spacing = 8
        stack.axis = .horizontal
        self.addSubview(stack)
        

        stack.snp.makeConstraints { make in
            make.leading.equalTo(snp.leading).offset(16)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.trailing.equalTo(snp.trailing).offset(-16)
            
            flagImageView.snp.makeConstraints { make in
                make.height.equalTo(20)
                make.width.equalTo(30)

            }

        }
    }
    
    private func resetData() {
        self.flagImageView.image = nil
        self.nameLabel.text = nil
        self.codeLabel.text = nil
    }
    
    func configureCell(country: Countries) {
        self.flagImageView.setWith(country.image)
        self.nameLabel.text = country.name
        self.codeLabel.text = country.code
    }
}
