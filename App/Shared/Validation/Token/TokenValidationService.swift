//
//  TokenValidationService.swift
//  App
//
//  Created by MGAbouarab on 12/01/2024.
//

import Foundation

struct TokenValidationService {
    static func validate(token: String?) throws -> String {
        guard let token = token?.trimWhiteSpace(), !token.isEmpty else {
            throw TokenValidationErrors.unAuthorized
        }
        return token
    }
}
