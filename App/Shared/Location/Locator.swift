////
////  Locator.swift
////  App
////
////  Created by MGAbouarab on 06/01/2024.
////
//
//import CoreLocation
//
//protocol LocatorDelegate: AnyObject {
//    func updateUserLocation(latitude: Double, longitude: Double)
//    func showLocationAlert(message: String)
//}
//final class Locator: NSObject {
//    
//    //MARK: - Properties
//    private weak var delegate: LocatorDelegate?
//    private var locationManger = CLLocationManager()
//    private(set) var usersCurrentLocation:CLLocationCoordinate2D? {
//        didSet {
//            if let latitude = self.usersCurrentLocation?.latitude , let longitude = self.usersCurrentLocation?.longitude {
//                LocationHelper.getAddressFrom(latitude: latitude, longitude: longitude) { [weak self] address in
//                    guard let _ = self, let address = address else {return}
//                    Locator.currentLocation = .init(latitude: latitude, longitude: longitude, address: address)
//                }
//                let latitude = Double(latitude)
//                let longitude = Double(longitude)
//                delegate?.updateUserLocation(latitude: latitude, longitude: longitude)
//            }
//        }
//    }
//    private(set) static var currentLocation: Location?
//    
//    //MARK: - init
//    init(delegate: LocatorDelegate) {
//        super.init()
//        self.delegate = delegate
//        
//        //location Handeling
//        self.locationManger.delegate = self
//        self.locationManger.requestWhenInUseAuthorization()
//        self.locationManger.desiredAccuracy = kCLLocationAccuracyBest
//        self.locationManger.startUpdatingLocation()
//    }
//    
//    //MARK: - Deinit -
//    deinit {
//        print("\(self.className) is deinit, No memory leak found")
//    }
//    
//    
//}
//extension Locator: CLLocationManagerDelegate {
//    
//    private enum LocationErrors {
//        case disabled
//        case denied
//        case restricted
//        
//        var message: String {
//            switch self {
//            case .disabled:
//                return "Please enable GPS service \n Settings > Privacy > Location services".localized
//            case .denied:
//                return "Please enable the app to access your location \n Settings > ".localized + "".localized + " > Location".localized
//            case .restricted:
//                return "The app is restricted from access your Location, please connect to your administrator to active it".localized
//            }
//        }
//    }
//    
//    private func checkLocationAuthorization() {
//        switch locationManger.authorizationStatus {
//        case .authorizedWhenInUse, .authorizedAlways:
//            self.locationManger.startUpdatingLocation()
//        case .notDetermined:
//            self.locationManger.requestWhenInUseAuthorization()
//        case .denied:
//            self.delegate?.showLocationAlert(message: LocationErrors.denied.message)
//        case .restricted:
//            self.delegate?.showLocationAlert(message: LocationErrors.restricted.message)
//        @unknown default:
//            break
//        }
//    }
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        guard status == .authorizedWhenInUse || status == .authorizedAlways else {
//            locationManger.requestWhenInUseAuthorization()
//            return
//        }
//        locationManger.startUpdatingLocation()
//    }
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard locations.first != nil else {
//            return
//        }
//        if let lat = locationManger.location?.coordinate.latitude , let long = locationManger.location?.coordinate.longitude {
//            self.usersCurrentLocation = CLLocationCoordinate2D(latitude: lat, longitude: long)
//        }
//        locationManger.stopUpdatingLocation()
//    }
//    
//}
