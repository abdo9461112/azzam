//
//  IntroSelectLanguageViewController.swift
//  App
//
//  Created by MGAbouarab on 26/12/2023.
//

import UIKit

class IntroSelectLanguageViewController: BaseViewController {
    
    //MARK: - Properties -
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    var globalModel: GlobalDataModel?
    var languages: [IntroSelectLanguage] = [
        .init(
            image: "arabicFlag",
            displayedName: "اللغة العربية",
            localeSymbol: Language.Languages.ar,
            isSelected: Language.isRTL()
        ),
        .init(
            image: "englishFlag",
            displayedName: "English",
            localeSymbol: Language.Languages.en,
            isSelected: !Language.isRTL()
        )
    ]
    var myView: IntroSelectLanguageView {
        return self.view as! IntroSelectLanguageView
    }
    
    //MARK: - Lifecycle -
    override func loadView() {
        let view = IntroSelectLanguageView()
        view.delegate = self
        self.view = view
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.myView.onConfirm = { [weak self] in
            guard let self else {return}
            guard let selectedLanguage = self.languages.first(where: { $0.isSelected }) else {
                print("Error:: \(#file)\n Try to continue without select any language!")
                return
            }
            Language.setAppLanguage(lang: selectedLanguage.localeSymbol)
           
        }
    }
    
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
}

extension IntroSelectLanguageViewController: IntroSelectLanguageViewDelegate {
    func didSelect(languageAtIndex index: Int) {
        for index in self.languages.indices {
            self.languages[index].isSelected = false
        }
        self.languages[index].isSelected = true
    }
}

extension IntroSelectLanguageViewController {
    private func goToNext() {
        Task {
            let endpoint = SettingsEndpoints.intro()
            do {
                guard let response = try await self.request(endpoint) else {return}
                let items = response.data ?? []
                let vc = OnBoardingPageController(items: items)
                AppHelper.changeWindowRoot(vc: vc)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
