//
//  IntroSelectLanguageView.swift
//  App
//
//  Created by MGAbouarab on 26/12/2023.
//

import UIKit

class IntroSelectLanguageView: UIView {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var collectionView: UICollectionView!
    
    @IBOutlet weak var lable: UILabel!
    //MARK: - Properties -
    weak var delegate: IntroSelectLanguageViewDelegate?
    private var languages: [IntroSelectLanguage] {
        get {
            self.delegate?.languages ?? []
        }
    }
    private let padding: CGFloat = 16
    private let spacing: CGFloat = 10 //Xib
    var onConfirm: (()->())?
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupCollectionView()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupCollectionView()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "IntroSelectLanguageView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - IBAction -
    @IBAction private func confirmButtonPressed() {
        self.onConfirm?()
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}


//MARK: - CollectionView -

extension IntroSelectLanguageView {
    func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(cellType: IntroSelectLanguageCollectionViewCell.self)
        self.collectionView.contentInset = .init(top: 0, left: padding, bottom: 0, right: padding)
    }
}

extension IntroSelectLanguageView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.languages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: IntroSelectLanguageCollectionViewCell.self, for: indexPath)
        cell.set(language: languages[indexPath.item])
        return cell
    }
}

extension IntroSelectLanguageView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect(languageAtIndex: indexPath.row)
        self.collectionView.reloadData()
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

extension IntroSelectLanguageView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let languageCount: CGFloat = 2//CGFloat(languages.count)
        let allSpacing = spacing*languageCount-1
        let length = (collectionView.bounds.width - 2*padding - allSpacing)/languageCount
        return .init(
            width: length,
            height: length
        )
    }
}
