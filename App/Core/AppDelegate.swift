//
//  AppDelegate.swift
//  App
//
//  Created by MGAbouarab on 20/12/2023.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FirebaseCore
import FirebaseMessaging
import IQKeyboardManagerSwift
import Network

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    //MARK: - Properties -
    let reachability: Reachability = Reachability()
    let gcmMessageIDKey = "gcm.message_id"
    var googleMapKey: String { UserDefaults.googleMapKey }
    
    //MARK: - Initializer -
    override init() {
        super.init()
        UIFont.overrideInitialize()
        UIFont.overrideSystemFont()
    }
    
    //MARK: - Application Lifecycle -
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //MARK: - Networking -
        self.startObserveNetworkChange()
        
        //MARK: - Keyboard -
        self.handleKeyboard()
        
        //MARK: - Google Map Key -
        self.handleGoogleMap()
        
        
        //MARK: - FCM -
        let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "NetworkMonitor")
        monitor.start(queue: queue)
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                DispatchQueue.main.async {
                    self.handleFCMFor(application)
                    queue.suspend()
                }
            } else {
                print("No network connection")
            }
        }
        
        return true
    }

    // MARK: - UISceneSession Lifecycle -
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {}

}

extension AppDelegate {
    
    //MARK: - FCM -
    private func handleFCMFor(_ application: UIApplication) {
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
    }
    
    //MARK: - Networking -
    private func startObserveNetworkChange() {
        reachability.listen { [weak self] status in
            switch status {
            case .unknown:
                break
            case .notReachable:
                ConnectionAlertManager.shared.showConnectionLost()
            case .reachable(_):
                guard self?.reachability.lastStatus == .notReachable else {return}
                ConnectionAlertManager.shared.showConnectionRestored()
            }
        }
    }
    
    //MARK: - Google Maps -
    private func handleGoogleMap() {
        GMSServices.provideAPIKey(self.googleMapKey)
        GMSPlacesClient.provideAPIKey(self.googleMapKey)
    }
    
    //MARK: - Keyboard -
    private func handleKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarConfiguration.tintColor = .main
        IQKeyboardManager.shared.resignOnTouchOutside = true
    }

      
    
}
