//
//  IntroSelectLanguageModel.swift
//  App
//
//  Created by MGAbouarab on 26/12/2023.
//

import Foundation

struct IntroSelectLanguage {
    let image: String
    let displayedName: String
    let localeSymbol: String
    var isSelected: Bool = false
}
