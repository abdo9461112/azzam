//
//  AppHelper.swift
//  App
//
//  Created by MGAbouarab on 30/12/2023.
//

import UIKit
import CoreLocation
import MapKit

struct AppHelper {
    
    private init() {}
    
    static func changeWindowRoot(vc: UIViewController, options: UIView.AnimationOptions = .transitionCrossDissolve) {
        DispatchQueue.main.async {
            SceneDelegate.current?.window?.rootViewController = vc
            guard let window = SceneDelegate.current?.window else {return}
            UIView.transition(with: window, duration: 0.3, options: options, animations: nil, completion: nil)
        }
    }
    static func openUrl(_ url: String?) {
        guard let stringUrl = url else {return}
        if let url = URL(string: stringUrl) {
            UIApplication.shared.open(url)
        }
    }
    
    
}

