//
//  HomeCenterTableViewCell.swift
//  App
//
//  Created by Abdo Emad on 17/07/2024.
//

import UIKit

class HomeCenterTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var continerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        continerView.layer.borderWidth = 1
    }

    func set(_ data : CenterGroupsData){
        icon.downloadImageWithTintColor(url: data.icon)
        nameLable.text = data.name
        nameLable.textColor = UIColor.init(hex: data.color)
        continerView.layer.borderColor = UIColor(hex: data.color).cgColor
        icon.tintColor = UIColor.init(hex: data.color)
        
    }
    
}
