//
//  HomeTableViewCell.swift
//  App
//
//  Created by Abdo Emad on 15/07/2024.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var titleStackView: UIStackView!
    @IBOutlet weak var moreStackView: UIStackView!
    private var homeModel: HomeDataModel?
    var moreAction = { }
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }

    func setUp(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(cellType: HomeSectionCollectionViewCell.self)
        collectionView.register(cellType: HomeDepartmentCollectionViewCell.self)
        collectionView.register(cellType: HomeLiveStreamCollectionViewCell.self)
        collectionView.backgroundColor = .clear
        let moreViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(moreViewTapped))
        moreStackView.addGestureRecognizer(moreViewTapGesture)
    }
    @objc func moreViewTapped(){
        moreAction()
    }
    
    func configureCell(forRow row: HomeDataModel) {
        if row.key == "departments" {
            titleLabel.isHidden = true
            moreLabel.text = "allDepartments".localized
            collectionView.collectionViewLayout = createDepartmentCollectionViewLayout()
        } else if row.key == "liveStreams" {
            titleLabel.isHidden = false
            titleLabel.text = "liveBroadcast".localized
            moreLabel.text = "more".localized
            collectionView.collectionViewLayout = createLiveStreamCollectionViewLayout()
        } else if row.key == "fixedDepartments" {
            titleStackView.isHidden = true
            collectionView.collectionViewLayout = createSectionCollectionViewLayout()
        }
        homeModel = row
        collectionView.reloadData()
    }
    
    //MARK: Create Department CollectionView Layout
    private func createDepartmentCollectionViewLayout() -> UICollectionViewCompositionalLayout {
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.8), heightDimension: .absolute(96)))
        item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4)
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.8), heightDimension: .absolute(80)), subitem: item, count: 1)
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuous
        
        return UICollectionViewCompositionalLayout(section: section)
    }
    
    //MARK: Create Live Stream CollectionView Layout
    private func createLiveStreamCollectionViewLayout() -> UICollectionViewCompositionalLayout {
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.8), heightDimension: .absolute(96)))
        item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4)
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.8), heightDimension: .absolute(96)), subitem: item, count: 1)
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuous
        
        return UICollectionViewCompositionalLayout(section: section)
    }
    
    //MARK: Create Section CollectionView Layout
    private func createSectionCollectionViewLayout() -> UICollectionViewCompositionalLayout {
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(88)))
        item.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4)
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .absolute(88)), subitem: item, count: 1)
        
        let section = NSCollectionLayoutSection(group: group)
        
        
        return UICollectionViewCompositionalLayout(section: section)
    }
}


extension HomeTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        homeModel?.list.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if homeModel?.key == "departments" {
            let cell = collectionView.dequeueReusableCell(with: HomeDepartmentCollectionViewCell.self, for: indexPath)
            if let departments = homeModel?.list[indexPath.row]{
                cell.configureCell(department: departments)
            }
            return cell
        } else if homeModel?.key == "liveStreams"{
            let cell = collectionView.dequeueReusableCell(with: HomeLiveStreamCollectionViewCell.self, for: indexPath)
            if let liveStream = homeModel?.list[indexPath.item] {
                cell.configureCell(liveStream: liveStream)
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(with: HomeSectionCollectionViewCell.self, for: indexPath)
            if let section = homeModel?.list[indexPath.item] {
                cell.configureCell(section: section)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if homeModel?.key == "fixedDepartments" {
            let item = homeModel?.list[indexPath.item]
            guard let title = item?.name,
                  let id = item?.id,
                  let slug = item?.slug else {return}
            let vc = HomeCenterVC.create(title: title, id: id, slug: slug)
            parentViewController()?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
