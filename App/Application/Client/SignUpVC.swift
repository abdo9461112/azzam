//
//  SignUpVC.swift
//  App
//
//  Created by Abdo Emad on 28/06/2024.
//

import UIKit

class SignUpVC: BaseViewController {
    
    @IBOutlet weak var userName: NormalTextFieldView!
    @IBOutlet weak var userPhone: PhoneTextFieldView!
    @IBOutlet weak var userEmail: EmailTextFieldView!
    @IBOutlet weak var userCity: DropDownTextFieldView!
    @IBOutlet weak var checkButton: UIButton!
    
    private let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var checkTerms = 0
    private var cityId: String?
    private var citiesModel: [City] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        fetchCountryCodes()
    }
    
    private func setUp() {
        addBackButtonWith(title: "Sign Up".localized)
        userCity.delegate = self
        userPhone.addTapGesture {
            self.showCountryCodePicker()
        }
    }
    
    
    private func setInitialCountryCode(_ country: Countries) {
        userPhone.countryCodeLabel.text = country.code
        userPhone.flagImage.setWith(country.image)
        getCities(countryId: country.id)
    }
    
    //MARK: - Actions
    
    @IBAction func checkButtonTapped(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            checkTerms = 1
            checkButton.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
            
        } else {
            checkTerms = 0
            checkButton.setImage(UIImage(systemName: "square"), for: .normal)
        }
    }
    
    @IBAction func termsButtonTapped(_ sender: UIButton) {
        let vc = TermsVC()
        push(vc)
    }
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        do {
            let name = try ValidationService.validate(name: userName.textValue())
            let phone = try userPhone.phoneText()
            let email = try userEmail.emailText()
            let cityId = try ValidationService.validate(cityId: cityId)
            let countryCode = try userPhone.countryCodeText()
            guard checkTerms == 1 else {
                show(errorMessage: "You must accept terms and conditions first".localized)
                return
            }
            signUp(name: name, phone: phone, countryCode: countryCode, email: email, cityId: cityId)
        } catch {
            show(errorMessage: error.localizedDescription)
        }
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        pop()
    }
    
    @IBAction func visitorButtonTapped(_ sender: UIButton) {
        UserDefaults.isFirstTime = true
        UserDefaults.isLogin = false
        let vc = UserTabBarController()
        AppHelper.changeWindowRoot(vc: vc)
    }
    
    
}

//MARK: - Networking

extension SignUpVC {
    
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func signUp(name: String, phone: String, countryCode: String, email: String, cityId: String) {
        Task {
            showIndicator()
            let endpoint = AuthEndpoints.signUp(name: name, phone: phone, countryCode: countryCode, email: email, cityId: cityId)
            do {
                guard let response = try await request(endpoint) else { return }
                hideIndicator()
                if response.key == .success {
                    show(successMessage: response.message)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let vc = ActivationCodeVC.sendToActivationCodeVC(phone: phone, countryCode: countryCode, vcType: .loginVC)
                        let nav = UINavigationController(rootViewController: vc)
                        AppHelper.changeWindowRoot(vc: nav)
                    }
                } else {
                    show(errorMessage: response.message)
                }
            } catch {
                print("error calling signUp request API: \(error.localizedDescription)")
            }
        }
    }
    
    private func getCities(countryId: String) {
        Task {
            let endpoint = SettingsEndpoints.getCities(countryId: countryId)
            do {
                guard let response = try await request(endpoint) else { return }
                citiesModel = response.data ?? []
            } catch {
                print("error trying to call get Cities API: \(error.localizedDescription)")
            }
        }
    }
    private func showCountryCodePicker() {
        Task {
            let endpoint = SettingsEndpoints.getCountries()
            do {
                guard let response = try await request(endpoint) else { return }
                let vc = CountriesVC(countries: response.data ?? [], delegate: self)
                let nav = BaseNavigationController(rootViewController: vc)
                self.present(nav, animated: true)
            } catch {
                print("error trying to call get Countries API: \(error.localizedDescription)")
            }
        }
    }
    
    private func fetchCountryCodes() {
        Task {
            let endpoint = SettingsEndpoints.getCountries()
            do {
                guard let response = try await request(endpoint) else { return }
                let countries = response.data ?? []
                userPhone.set(countryCodes: countries)
                if let firstCountry = countries.first {
                    setInitialCountryCode(firstCountry)
                }
            } catch {
                print("error trying to call get Countries API: \(error.localizedDescription)")
            }
        }
    }
}

//MARK: - CountryCodeDelegate & DropDownTextFieldViewDelegate

extension SignUpVC: CountryCodeDelegate, DropDownTextFieldViewDelegate {
    
    func didSelectCountry(_ item: Countries) {
        userPhone.countryCodeLabel.text = item.code
        userPhone.flagImage.setWith(item.image)
        getCities(countryId: item.id)
    }
    
    func dropDownList(for textFieldView: DropDownTextFieldView) -> [any DropDownItem] {
        return citiesModel
    }
    
    func didSelect(item: any DropDownItem, for textFieldView: DropDownTextFieldView) {
        cityId = item.id
    }
}
