//
//  TextFieldView.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import UIKit

class TextFieldView: UIView {
    
    let activeTintColor: UIColor = .main
    let inActiveTintColor: UIColor = .secondaryLabel
    
    let containerViewCornerRadius: CGFloat = 16
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}

class PickerView: TextFieldView {
    
    let textFieldTintColor: UIColor = .clear
    let doneToolbarTintColor: UIColor = .main
    let picketTitleColor: UIColor = .label
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}

