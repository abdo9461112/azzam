////
////  OrderEndpoints.swift
////  App
////
////  Created by MGAbouarab on 11/01/2024.
////
//
//import Foundation
//
//struct OrderEndpoints {
//    private init() {}
//}
//
//extension OrderEndpoints {
//    
//    //MARK: - Parcel Order -
//    static func parcelDeliveryPrice(
//        receiveLocation: Location,
//        deliverLocation: Location,
//        coupon: String?
//    ) -> Endpoint<BaseResponse<OrderPriceModel>> {
//        return .init(
//            method: .post,
//            path: "order-enquiry",
//            body: [
//                "receive_lat" : receiveLocation.latitude,
//                "receive_long" : receiveLocation.longitude,
//                "deliver_lat" : deliverLocation.latitude,
//                "deliver_long" : deliverLocation.longitude,
//                "coupon": coupon
//            ]
//        )
//    }
//    static func createParcelDeliveryOrder(
//        byDetails details: String,
//        andImages imageData: [UploadData],
//        forPaymentType paymentType: String,
//        fromLocation startLocation: Location,
//        toLocation endLocation: Location,
//        byHoursCount hours: Int,
//        byCoupon coupon: String?,
//        token: String
//    ) -> Endpoint<BaseResponse<SuccessOrderModel>> {
//        return .init(
//            method: .post,
//            path: "create-order",
//            body: [
//                "description": details,
//                "payment_type" : paymentType,
//                "type" : "parcel_delivery",
//                "receive_lat" : startLocation.latitude,
//                "receive_long" : startLocation.longitude,
//                "receive_address" : startLocation.address,
//                "deliver_lat" :endLocation.latitude,
//                "deliver_long" : endLocation.longitude,
//                "deliver_address": endLocation.address,
//                "deliver_time": hours,
//                "coupon": coupon,
//                "needs_delivery" : true
//            ],
//            headerType: .authorized(token: token),
//            uploads: imageData
//        )
//    }
//    
//    //MARK: - Special Order -
//    static func createSpecialOrder(
//        byDetails details: String,
//        andImages imageData: [UploadData],
//        forPaymentType paymentType: String,
//        toLocation endLocation: Location,
//        byHoursCount hours: Int,
//        byCoupon coupon: String?,
//        token: String
//    ) -> Endpoint<BaseResponse<SuccessOrderModel>> {
//        return .init(
//            method: .post,
//            path: "create-order",
//            body: [
//                "description": details,
//                "payment_type" : paymentType,
//                "type" : "special_request",
//                "deliver_lat" :endLocation.latitude,
//                "deliver_long" : endLocation.longitude,
//                "deliver_address": endLocation.address,
//                "deliver_time": hours,
//                "coupon": coupon,
//                "needs_delivery" : true
//            ],
//            headerType: .authorized(token: token),
//            uploads: imageData
//        )
//    }
//    
//    //MARK: - Normal Order -
//    static func normalOrderPrice(
//        fromStoreId storeId: Int,
//        withStoreLocation storeLocation: Location,
//        toClientLocation clientLocation: Location,
//        coupon:String?,
//        price : Double
//    ) -> Endpoint<BaseResponse<OrderPriceModel>> {
//        return .init(
//            method: .post,
//            path: "order-enquiry",
//            body: [
//                "store_id": storeId,
//                "receive_lat" : storeLocation.latitude,
//                "receive_long" : storeLocation.longitude,
//                "deliver_lat" : clientLocation.latitude,
//                "deliver_long" : clientLocation.longitude,
//                "price": price,
//                "coupon": coupon
//            ]
//        )
//    }
//    static func createNormalOrder(
//        fromStore store: ProviderModel,
//        byDetails details: String,
//        andGroups groups: String,
//        forPaymentType paymentType: String,
//        toLocation endLocation: Location?,
//        byHoursCount hours: Int?,
//        byCoupon coupon: String?,
//        token: String
//    ) -> Endpoint<BaseResponse<SuccessOrderModel>> {
//        return .init(
//            method: .post,
//            path: "create-order",
//            body: [
//                "store_id": "\(store.id)",
//                "store_name": store.name,
//                "store_icon": store.icon,
//                "groups": groups,
//                "description": details,
//                "payment_type" : paymentType,
//                "type" : "special_stores",
//                "receive_lat" : store.latitude,
//                "receive_long" : store.longitude,
//                "receive_address" : store.address,
//                "deliver_lat" :endLocation?.latitude,
//                "deliver_long" : endLocation?.longitude,
//                "deliver_address": endLocation?.address,
//                "deliver_time": hours,
//                "coupon": coupon,
//                "needs_delivery" : "\(endLocation != nil)"
//            ],
//            headerType: .authorized(token: token)
//        )
//    }
//    
//}
