import UIKit
import ImageSlideshow
import Kingfisher

final class HomeViewController: BaseViewController {
    
    //MARK: - IBOutlets -
    
    @IBOutlet weak var tableView: UITableView!
    //MARK: - Properties -
    let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var homeModel : [HomeDataModel] = [] {
        didSet{
            tableView.reloadData()
        }
    }
    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        getHome()
        setUp()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (self.navigationController as? ColoredNav)?.changeApperance(to: .colored)
    }
    
    private func setUp() {
        setLeading(title: "Home_page_title_tabBar".localized, color: .white)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: HomeSliderCell.self)
        tableView.register(cellType: HomeTableViewCell.self)
        
    }
    
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        homeModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = homeModel[indexPath.row]
        if row.key == "sliders" {
            let cell = tableView.dequeueReusableCell(with: HomeSliderCell.self, for: indexPath)
            cell.configureCell(forRow: row)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(with: HomeTableViewCell.self, for: indexPath)
            cell.configureCell(forRow: row)
            return cell
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let row = homeModel[indexPath.row]
        if row.key == "sliders" {
            return 200
        }else{
            return 400
        }
    }
    

}


extension HomeViewController {
    func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getHome() {
        Task {
            showIndicator()
            let endpoint = HomeEndPoints.getHome()
            do {
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                guard let data = response.data else {return}
                homeModel = data
            } catch {
                print("Error trying to call get home API request: \(error.localizedDescription)")
            }
        }
    }
}

