//
//  HomeEndPoints.swift
//  App
//
//  Created by MGAbouarab on 04/01/2024.
//

import Foundation

struct HomeEndPoints {
    private init() {}
}

extension HomeEndPoints {
    
    static
    func getHome() -> Endpoint<BaseResponse<[HomeDataModel]>>{
        return.init(path: "home")
    }
    static
    func getConsultationGroups(id : String, name : String)  -> Endpoint<BaseResponse<CenterGroupsModel>>{
        return.init(path: "consultation-groups",
                    queries:
                        [
                            "department":id,
                            "slug":name
                        ]
        )
    }
    static
    func getCenters(id : String, name : String)  -> Endpoint<BaseResponse<CenterGroupsModel>>{
        return.init(path: "centers",
                    queries:
                        [
                            "department":id,
                            "slug":name
                        ]
        )
    }
    static
    func getCentersDetails(id : String)  -> Endpoint<BaseResponse<CenterGroupsModel>>{
        return.init(path: "center-details",
                    queries:
                        [
                            "department":id,
                        ]
        )
    }
    
}
