//
//  MedicalRecordsVC.swift
//  App
//
//  Created by Abdo Emad on 29/06/2024.
//

import UIKit
import SnapKit

class MedicalRecordsVC: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private var currentPage: Int = 1
    private var isLastPage: Bool = false
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var recordsModel : [MedicalRecordsModel] = [ ]{
        didSet{
            tableView.reloadData()
            if recordsModel.isEmpty{
                emptyMessage.isHidden = false
            }else{
                emptyMessage.isHidden = true
            }
        }
    }
    
    private let emptyMessage : UILabel = {
        let lable = UILabel()
        lable.text = "Empty data".localized
        lable.font = UIFont.systemFont(ofSize: 18)
        return lable
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        switch segmentedControl.selectedSegmentIndex{
        case 0:
            getMedicalRecords(type: "treatmentPlan")
        case 1:
            getMedicalRecords(type: "urgentConsultation")
        default:
            break
        }
    }

    private func setUp(){
        addBackButtonWith(title: "Medical records".localized)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: MedicalRecordsCell.self)
        view.addSubview(emptyMessage)
        emptyMessage.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
    }
}

//MARK: - TableView

extension MedicalRecordsVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: MedicalRecordsCell.self, for: indexPath)
        cell.setData(recordsModel[indexPath.row])
        switch segmentedControl.selectedSegmentIndex{
        case 0 :
            cell.title.text  = "Plan Number".localized
            cell.subTitle.text = "Plan report".localized
        case 1 :
            cell.title.text  = "Consultation number".localized
            cell.subTitle.text = "Consultation report".localized
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        150
    }
    
    
}
//MARK: - NetWoring


extension MedicalRecordsVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    
    private func getMedicalRecords(type: String){
        Task{
            let endpoint = SettingsEndpoints.getMedicalRecords(type: type, page: currentPage)
            do{
                guard let response = try await request(endpoint)else{return}
                guard let data = response.data, let pagination = response.paginate else { return }
                if currentPage == 1{
                    recordsModel = data
                }else{
                    recordsModel.append(contentsOf: data)
                }
                isLastPage = currentPage == pagination.lastPage
                currentPage += 1
            }catch{
                print("error trying to call  getMedicalRecords api request \(error.localizedDescription)")
            }
        }
    }
}




