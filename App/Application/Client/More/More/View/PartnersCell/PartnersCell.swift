//
//  PartnersCell.swift
//  App
//
//  Created by Abdo Emad on 08/07/2024.
//

import UIKit

class PartnersCell: UICollectionViewCell {

    @IBOutlet weak var continerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        continerView.layer.borderWidth = 1
        continerView.layer.borderColor = .mainCGColor
    }
    
    func setData(_ data : PartnersModel){
        name.text = data.name
        imageView.setWith(data.image)
    }

}
