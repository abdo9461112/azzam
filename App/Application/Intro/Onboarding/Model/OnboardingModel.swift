//
//  OnboardingModel.swift
//  App
//
//  Created by MGAbouarab on 29/12/2023.
//

import Foundation

struct OnboardingData: Decodable {
    let intros: [OnboardingModel]
}

struct OnboardingModel: Decodable {
    let id : String
    let image: String
    let title: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case image
        case title
        case description 
    }
    
    init(from decoder: any Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.image = try container.decode(String.self, forKey: .image)
        self.title = try container.decode(String.self, forKey: .title)
        self.description = try container.decode(String.self, forKey: .description)
    }
}
