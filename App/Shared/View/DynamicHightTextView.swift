//
//  DynamicHightTextView.swift
//  App
//
//  Created by MGAbouarab on 11/01/2024.
//

import UIKit

class DynamicHightTextView: UITextView {
    
    private let minimumHeight: CGFloat = 100
    var allowDynamic: Bool = false
    
    override var contentSize:CGSize {
        didSet {
            guard allowDynamic else {return}
            self.invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        guard allowDynamic else {return super.intrinsicContentSize}
        self.layoutIfNeeded()
        let height = max(contentSize.height, minimumHeight)
        return CGSize(width: UIView.noIntrinsicMetric, height: height)
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}



