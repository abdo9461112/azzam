//
//  HomeSectionCollectionViewCell.swift
//  App
//
//  Created by Abdo Emad on 15/07/2024.
//

import UIKit

class HomeSectionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var sectionBackgroundView: UIView!
    @IBOutlet weak var sectionIcon: UIImageView!
    @IBOutlet weak var sectionTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(section: HomeData) {
        if let color = section.color {
            sectionBackgroundView.backgroundColor = UIColor(hex: color)
        }
        sectionIcon.setWith(section.icon)
        sectionTitleLabel.text = section.name
    }

}
