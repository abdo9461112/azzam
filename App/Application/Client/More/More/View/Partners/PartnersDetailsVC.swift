//
//  PartnersDetailsVC.swift
//  App
//
//  Created by Abdo Emad on 08/07/2024.
//

import UIKit

class PartnersDetailsVC: BaseViewController {
    @IBOutlet weak var continerView: UIView!
    @IBOutlet weak var partnerImage: UIImageView!
    @IBOutlet weak var partnerName: UILabel!
    @IBOutlet weak var partnerDec: UITextView!
    
    private let responseHandler: ResponseHandler = DefaultResponseHandler()
    var partnerId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        getParnterDetails(id: partnerId)
    }

    private func setUp(){
        addBackButtonWith(title: "Partners".localized)
        continerView.layer.borderWidth = 1
        continerView.layer.borderColor = .mainCGColor
    }
    static func create(id: String) -> PartnersDetailsVC{
        let vc = PartnersDetailsVC()
        vc.partnerId = id
        return vc
    }


}

//MARK: - NetWorking

extension PartnersDetailsVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
         return try await self.responseHandler.get(endPoint, progress: progress)
     }
    private func getParnterDetails(id:String){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.getPartnersDetails(partnerId: id)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                partnerName.text = response.data?.name
                partnerDec.text = response.data?.description
                partnerImage.setWith(response.data?.image)
            }catch{
                
            }
        }
    }
}



