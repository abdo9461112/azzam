//
//  NotificationsViewController.swift
//  App
//
//  Created by MGAbouarab on 07/01/2024.
//

import UIKit
import Combine

final class NotificationsViewController: BaseViewController {

    //MARK: - IBOutlets -
    @IBOutlet weak private var tableView: UITableView!
    
    //MARK: - Properties -
    private let viewModel: NotificationsViewModel
    private var cancellable: Set<AnyCancellable> = []
    
    //MARK: - Initializers -
    init(viewModel: NotificationsViewModel = NotificationsViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: "NotificationsViewController", bundle: nil)
        self.hidesBottomBarWhenPushed = true
    }
    required init?(coder: NSCoder) {
        self.viewModel = NotificationsViewModel()
        super.init(coder: coder)
    }
    
    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationDesign()
        self.setupTableView()
        self.addBinding()
        self.viewModel.startGettingNotifications()
    }
    
    //MARK: - Design -
    private func setupNavigationDesign() {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.addBackButtonWith(title: "Notification_title_page".localized)
    }
    
    //MARK: - Binding -
    private func addBinding() {
        self.viewModel.$notifications
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.tableView.reloadData()
            }
            .store(in: &cancellable)
    }

}

//MARK: - UITableView -
extension NotificationsViewController {
    
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(cellType: NotificationTableViewCell.self)
        self.tableView.showsVerticalScrollIndicator = false
    }
    
}
extension NotificationsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.notifications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: NotificationTableViewCell.self, for: indexPath)
        let notification = self.viewModel.notifications[indexPath.row]
        cell.set(notification)
        return cell
    }
    
}
extension NotificationsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYOffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYOffset

        if distanceFromBottom < height {
            self.viewModel.loadMore()
        }
    }
    
}
