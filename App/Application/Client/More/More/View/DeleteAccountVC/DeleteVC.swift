//
//  DeleteAccountVC.swift
//  App
//
//  Created by Abdo Emad on 30/06/2024.
//

import UIKit
enum DeletionType{
    case deleteAccount
    case deleteCompanion
}
protocol DeleteViewControllerDelegate: AnyObject {
    func presenSuccessVC()
}

class DeleteVC: BaseViewController {
    @IBOutlet weak var titleLable: UILabel!
    
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var type : DeletionType = .deleteAccount
    weak var delegate: DeleteViewControllerDelegate?
    private var titleText: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLable.text = titleText
    }

    static func create(deletionType: DeletionType, title: String) -> DeleteVC {
        let vc = DeleteVC()
        vc.type = deletionType
        vc.titleText = title
        return vc
    }
    @IBAction func yesButtonTapped(_ sender: UIButton) {
        switch type {
        case .deleteAccount:
            deleteAccount()
        case .deleteCompanion:
            dismiss(animated: true) {[weak self] in
                self?.delegate?.presenSuccessVC()
            }
        }
    }
    
    @IBAction func noButtonTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
}

//MARK: - NetWorking

extension DeleteVC{
   private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func deleteAccount(){
        Task{
            let endpoint = SettingsEndpoints.deleteAccount()
            do{
                guard let response = try await request(endpoint)else{return}
                if response.key == .success{
                    UserDefaults.userData = nil
                    UserDefaults.isLogin = false
                    UserDefaults.isFirstTime = true
                    let vc = LoginVC()
                    let nav = BaseNavigationController(rootViewController: vc)
                    AppHelper.changeWindowRoot(vc: nav)
                }
            }catch{
                print("error trying to call delete account api request : \(error.localizedDescription)")
            }
        }
    }
    
}


