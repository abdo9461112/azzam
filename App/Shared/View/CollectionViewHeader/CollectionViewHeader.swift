//
//  CollectionViewHeader.swift
//  App
//
//  Created by MGAbouarab on 06/01/2024.
//


import UIKit

class CollectionViewHeader: UICollectionReusableView {

    //MARK: - IBOutlets -
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var actionButton: UIButton!
    
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    //MARK: - Initializer -
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.xibSetUp()
        self.setupInitialDesign()
    }
    
    private func xibSetUp() {
        let view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CollectionViewHeader", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.titleLabel.isHidden = true
        self.actionButton.isHidden = true
    }
    
    
    //MARK: - Data -
    func set(title: String?) {
        self.titleLabel.text = title
        self.titleLabel.isHidden = (title == nil || title == "")
    }
    func setAction(title: String?, selector: Selector?) {
        if let selector, let title {
            self.actionButton.isHidden = false
            self.actionButton.setTitle(title, for: .normal)
            self.actionButton.addTarget(nil, action: selector, for: .touchUpInside)
        } else {
            self.actionButton.isHidden = false
        }
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
