//
//  NotificationTableViewCell.swift
//  App
//
//  Created by MGAbouarab on 07/01/2024.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var iconImageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var descriptionLabel: UILabel!
    @IBOutlet weak private var dateLabel: UILabel!
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupInitialDesign()
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.selectionStyle = .none
        self.containerView.layer.cornerRadius = .cardCorner
        self.containerView.clipsToBounds = true
        self.containerView.tintColor = .main
    }
    
    //MARK: - Data -
    func set(_ data: NotificationData) {
        self.iconImageView.setWith(data.icon)
        self.titleLabel.text = data.title
        self.descriptionLabel.text = data.message
        self.dateLabel.text = data.createdAt
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
