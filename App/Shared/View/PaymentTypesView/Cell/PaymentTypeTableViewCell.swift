//
//  PaymentTypeTableViewCell.swift
//  App
//
//  Created by MGAbouarab on 09/01/2024.
//

import UIKit

class PaymentTypeTableViewCell: UITableViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var detailsLabel: UILabel!
    @IBOutlet weak private var iconImageView: UIImageView!
    @IBOutlet weak private var checkImageView: UIImageView!
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupInitialDesign()
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.selectionStyle = .none
        self.containerView.layer.cornerRadius = .smallCardCorner
    }
    
    //MARK: - Data -
    func set(_ data: PaymentType) {
        self.titleLabel.text = data.name
        self.detailsLabel.text = data.description
        self.iconImageView.setWith(data.image)
        self.containerView.backgroundColor = data.isSelected ? .main : .secondarySystemBackground
        self.checkImageView.isHidden = !data.isSelected
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}
