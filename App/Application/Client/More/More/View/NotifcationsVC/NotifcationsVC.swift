//
//  NotifcationsVC.swift
//  App
//
//  Created by Abdo Emad on 10/07/2024.
//

import UIKit

class NotifcationsVC: BaseViewController {

    @IBOutlet weak var switchButton: UISwitch!
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    private func setUp(){
        addBackButtonWith(title: "Notifications_Settings_More_Title".localized)
        switchButton.isOn = UserDefaults.userData?.isNotify ?? true
    }

    @IBAction func switchButtonTapped(_ sender: UISwitch) {
        notificationSwitch(isNotify: sender.isOn)
        
    }
    
}

extension NotifcationsVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
         return try await self.responseHandler.get(endPoint, progress: progress)
     }
    
    private func notificationSwitch(isNotify:Bool){
        Task{
            let endpoint = SettingsEndpoints.notificationSwitch(isNotify: isNotify)
            do{
                guard let response = try await request(endpoint)else{return}
                UserDefaults.userData = response.data
            }catch{
                print("Error trying to call notificationSwitch API request: \(error.localizedDescription)")

            }
        }
    }
}
