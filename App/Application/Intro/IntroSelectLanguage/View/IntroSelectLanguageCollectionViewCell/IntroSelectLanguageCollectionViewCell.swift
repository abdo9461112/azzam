//
//  IntroSelectLanguageCollectionViewCell.swift
//  App
//
//  Created by MGAbouarab on 26/12/2023.
//

import UIKit

class IntroSelectLanguageCollectionViewCell: UICollectionViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak private var flagImageView: UIImageView!
    @IBOutlet weak private var flagContainerView: UIView!
    @IBOutlet weak private var languageNameLabel: UILabel!
    
    //MARK: - Lifecycle -
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupInitialDesign()
    }
    
    //MARK: - Design -
    private func setupInitialDesign() {
        self.layer.cornerRadius = .cardCorner
        self.clipsToBounds = true
        self.layer.borderWidth = .defaultBorderWidth
    }
    
    //MARK: - Data -
    func set(language data: IntroSelectLanguage) {
        self.flagImageView.image = UIImage(named: data.image)
        self.languageNameLabel.text = data.displayedName
        self.layer.borderColor = data.isSelected ? UIColor.main.cgColor : UIColor.separator.cgColor
        self.backgroundColor = data.isSelected ? .main : .clear
        self.flagContainerView.backgroundColor = data.isSelected ? .systemBackground : .secondarySystemBackground
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
}
