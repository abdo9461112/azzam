//
//  UIImageView.swift
//  App
//
//  Created by MGAbouarab on 29/12/2023.
//

import UIKit.UIImageView
import Kingfisher

extension UIImageView {
    func setWith(_ stringURL: String?) {
        self.kf.indicatorType = .activity
        let placeholder = UIImage(named: "AppIcon")
        guard let stringURL, let url = URL(string: stringURL) else {
            self.image = UIImage(named: "AppIcon")
            return
        }
        self.kf.setImage(
            with: url,
            placeholder: placeholder,
            options: [
                .onFailureImage(placeholder),
                .transition(.fade(0.4))
            ]
        )
    }
    func downloadImageWithTintColor(url: String?){
        let modifier = AnyImageModifier { return $0.withRenderingMode(.alwaysTemplate) }
        
        if let url = URL(string: url ?? ""){
            let processor = DefaultImageProcessor()
            self.kf.indicatorType = .activity
            
            self.kf.setImage(
                with: url, placeholder: #imageLiteral(resourceName: "logo"), options: [
                    .processor(processor),
                    .transition(.fade(0.5)),
                    .cacheMemoryOnly,
                    .onFailureImage(#imageLiteral(resourceName: "logo")),
                    .scaleFactor(3),
                    .imageModifier(modifier)
                ])
        }else{
            self.image = #imageLiteral(resourceName: "logo")
        }
    }
}
