//
//  ActivationCodeVC.swift
//  App
//
//  Created by Abdo Emad on 28/06/2024.
//

import UIKit
enum VCType{
    case loginVC
    case profileVC
    case activateNewNumber
}

class ActivationCodeVC: BaseViewController {
    
    @IBOutlet weak var codeField: OTPTextField!
    
    private let responseHandler: ResponseHandler = DefaultResponseHandler()
    private var phone : String?
    private var code : String?
    private var countryCode : String?
    private var vcType : VCType = .loginVC
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        switch vcType {
        case .loginVC:
            setLeading(title: "Activation code".localized)
        case .profileVC:
            sendCode(phone: phone! , countryCode: countryCode!)
            addBackButtonWith(title: "Activation code".localized)
        case .activateNewNumber:
            addBackButtonWith(title: "Activation code".localized)
        }
    }
    private func setUp(){
        codeOTPConfigure()
        codeField.didEnterLastDigit = { code in
            self.code = code
        }
        codeField.becomeFirstResponder()
    }
    static func sendToActivationCodeVC(phone:String, countryCode: String,vcType: VCType) -> ActivationCodeVC{
        let vc = ActivationCodeVC()
        vc.phone = phone
        vc.countryCode = countryCode
        vc.vcType = vcType
        return vc
    }

    
    private func codeOTPConfigure() {
        codeField.configure(with: 6)
        codeField.didEnterLastDigit = { [weak self] code in
            guard let self = self else { return }
            self.codeField.resignFirstResponder()
        }
    }
    
    @IBAction func confirmButtonTapped(_ sender: UIButton) {
        switch vcType {
        case .loginVC:
            validateLogin()
        case .profileVC:
            validateOldNumber()
        case .activateNewNumber:
            validateNewNumber()
        }
    }
    
    @IBAction func resendCodeButtonTapped(_ sender: UIButton) {
        resendCode(phone: phone ?? " ", countryCode: countryCode!)
    }
    
    //MARK: - Validation
    
    private func validateLogin(){
        do{
            let code = try ValidationService.validate(verificationCode: code)
            activatetionCode(phone: phone!, code: code, countryCode: countryCode!)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
        UserDefaults.isFirstTime = false
        UserDefaults.isLogin = true
    }
    
    private func validateOldNumber(){
        do{
            let _ = try ValidationService.validate(verificationCode: code)
            let vc = ChangePhoneVC()
            push(vc)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
    private func validateNewNumber(){
        do{
            let code = try ValidationService.validate(verificationCode: code)
            activateNewPhoneNumber(phone: phone!, countryCode: countryCode!, code: code)
        }catch{
            show(errorMessage: error.localizedDescription)
        }
    }
}

extension ActivationCodeVC{
    
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func activatetionCode(phone: String,code:String, countryCode: String){
        Task{
            showIndicator()
            let endpoint = AuthEndpoints.activatetionCode(phone: phone, code: code, countryCode: countryCode)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    UserDefaults.userData = response.data
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let vc = UserTabBarController()
                        AppHelper.changeWindowRoot(vc: vc)
                    }
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                print("error calling activatetion Code api request : \(error.localizedDescription)")
            }
        }
    }
    
    private func resendCode(phone: String, countryCode: String){
        Task{
            let endpoint = AuthEndpoints.resendCode(phone: phone, countryCode: countryCode)
            do{
                guard let response = try await request(endpoint)else{return}
                show(successMessage: response.message)
            }catch{
                print("error calling resend Code api request : \(error.localizedDescription)")
                
            }
        }
    }
    private func sendCode(phone: String,countryCode: String){
        showIndicator()
        Task{
            let endpoint = AuthEndpoints.sendCode(phone: phone, countryCode: countryCode)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                show(successMessage: response.message)
            }catch{
                
            }
        }
    }
    private func activateNewPhoneNumber(phone: String,countryCode: String, code:String){
        Task{
            showIndicator()
            let endpoint = AuthEndpoints.activateNewPhoneNumber(phone: phone, countryCode: countryCode, code: code)
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                if response.key == .success{
                    show(successMessage: response.message)
                    popToRoot()
                }else{
                    show(errorMessage: response.message)
                }
            }catch{
                
            }
        }
    }
}
