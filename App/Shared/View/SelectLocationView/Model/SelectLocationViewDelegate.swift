////
////  SelectLocationViewDelegate.swift
////  App
////
////  Created by MGAbouarab on 11/01/2024.
////
//
//import Foundation
//
//protocol SelectLocationViewDelegate: AnyObject {
//    func didPickLocation(_ location: Location, for selectLocationView: SelectLocationView)
//}
