//
//  CommonQuestionsCell.swift
//  App
//
//  Created by Abdo Emad on 02/07/2024.
//

import UIKit

class CommonQuestionsCell: UITableViewCell {
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var decribtionLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    var onCliking : (()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setData(model: CommonQuestionsModel) {
        titleLable.text = model.question
        decribtionLabel.text = model.answer
        let isSelected = model.isSelected ?? false
        decribtionLabel.isHidden = !isSelected
        arrowImage.isHidden = isSelected
        if !isSelected {
            arrowImage.image = UIImage(systemName: "chevron.down")
        }
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        onCliking?()
    }
}
