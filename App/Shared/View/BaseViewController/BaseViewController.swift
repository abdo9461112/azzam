//
//  BaseViewController.swift
//  App
//
//  Created by MGAbouarab on 30/12/2023.
//

import UIKit


class BaseViewController: UIViewController {
    
    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .secondarySystemBackground
    }
    
    //MARK: - Design -
    func addBackButtonWith(title: String, color : UIColor = .label) {
        let button = UIButton()
        let image = UIImage(systemName: "chevron.backward")
        button.setImage(image, for: .normal)
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = .boldSystemFont(ofSize: 20)
        titleLabel.textColor = color
        button.isUserInteractionEnabled = false
        let stack = UIStackView.init(arrangedSubviews: [button, titleLabel])
        stack.axis = .horizontal
        stack.spacing = 8
        stack.tintColor = .label
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backButtonPressed))
        stack.addGestureRecognizer(tap)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: stack)
    }
    func setLeading(title: String?, color: UIColor = .label) {
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = .boldSystemFont(ofSize: 20)
        titleLabel.textColor = color
        let barButtonItem = UIBarButtonItem(customView: titleLabel)
        
        // Set the leading space
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        flexibleSpace.width = 16
        
        self.navigationItem.leftBarButtonItems = [flexibleSpace, barButtonItem]
    }
    
    func push(_ viewController: UIViewController, animated: Bool = true) {
        self.navigationController?.pushViewController(viewController, animated: animated)
    }
    func pop(animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }

    func popToRoot(animated: Bool = true) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    func presentOverFullScreen(_ vc: UIViewController, animated: Bool = true) {
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: animated)
    }
    func presentFullScreen(_ vc: UIViewController, animated: Bool = true) {
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: animated)
    }
    
    //MARK: - Actions -
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Deinit -
    deinit {
        print("\(self.className) is deinit, No memory leak found")
    }
    
}

extension BaseViewController {
    func showIndicator() {
        LoaderManager.shared.show()
    }
    func hideIndicator() {
        LoaderManager.shared.hide()
    }
}

extension BaseViewController {
    
    func show(successMessage message: String) {
        AlertManager.shared.show(message: message, type: .image(.init(systemName: "checkmark.square.fill")))
    }
    func show(errorMessage message: String) {
        let errorImage: UIImage? = .init(systemName: "xmark.app.fill")?.withTintColor(.systemRed, renderingMode: .alwaysOriginal)
        AlertManager.shared.show(message: message, type: .image(errorImage))
    }
    
    func showRequiredLoginAlert() {
        self.presentCancelableAlert(
            title: "Login_alert_title".localized,
            message: "Login_alert_message".localized,
            actionTitle: "Login_alert_action_title".localized) {
                let vc = LoginVC()
                let navController = UINavigationController(rootViewController: vc)
                AppHelper.changeWindowRoot(vc: navController)
            }
    }
    
    func presentCancelableAlert(title: String, message: String, actionTitle: String, handler: @escaping (() -> Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let titleAttributes = [NSAttributedString.Key.font: UIFont.appBold(size: 15), NSAttributedString.Key.foregroundColor: UIColor.label]
        let titleString = NSAttributedString(string: title, attributes: titleAttributes)
        let messageAttributes = [NSAttributedString.Key.font: UIFont.appRegular(size: 13), NSAttributedString.Key.foregroundColor: UIColor.secondaryLabel]
        let messageString = NSAttributedString(string: message, attributes: messageAttributes)
        
        alert.setValue(titleString, forKey: "attributedTitle")
        alert.setValue(messageString, forKey: "attributedMessage")
        
        let deleteAction = UIAlertAction(title: actionTitle, style: .destructive) { _ in
            handler()
        }
        let cancelAction = UIAlertAction(title: "alert_cancel_title".localized, style: .cancel)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = .main
        self.present(alert, animated: true)
    }
    
}




enum ViewState {
    case loading
    case error(error: Error)
}








































