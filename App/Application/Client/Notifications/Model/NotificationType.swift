//
//  NotificationType.swift
//  App
//
//  Created by MGAbouarab on 07/01/2024.
//

import Foundation

enum NotificationType: String, Decodable {
    
    // common
    case newReview = "new_review" // -> تقييم جديد
    case newMessage = "new_message" // -> رسالة جديدة
    case blockNotify = "block_notify" // -> تم حظر الحساب
    case deleteNotify = "delete_notify" // -> تم حذف الحساب
    case adminNotify = "admin_notify" // -> رسالة من الداشبورد
    
    // delegate
    case payWithWallet = "pay_with_wallet" // -> الدفع بالمحفظة
    case newOrder = "new_order" // -> هناك طلب جديد
    case userAcceptedOffer = "user_accepted_offer" // -> العميل وافق على العرض
    case userRejectedOffer = "user_rejected_offer" // ->العميل رفض العرض
    case changePaymentMethod = "change_payment_method" // -> تغيير طريقة الدفع
    case orderIsPaid = "order_is_paid" // -> تم الدفع
    case orderIsReady = "order_is_ready" // -> قام مقدم الخدمة بتجهيز الطلب
    case storeRefuseOrder = "store_refuse_order" // -> قام مقدم الخدمة برفض الطلب
    case userCancelOrder = "user_cancel_order" // -> قام العميل بالغاء الطلب

    // user
    case withdrawOrder = "withdraw_order" // -> المندوب انسحب من الطلب
    case acceptJoinRequest = "accept_join_request" // -> تم قبول طلب الانضمام ك مندوب
    case delegateFinishedOrder = "delegate_finished_order" // -> المندوب انهى الطلب
    case delegateMadeOffer = "delegate_made_offer" // -> المندوب قدم عرض
    case orderInTransit = "order_intransit" // -> الطلب قيد التوصيل
    case delegateCreatedInvoice = "delegate_created_invoice" // -> المندوب انشأ فاتورة
    case delegateAcceptedOffer = "delegate_accepted_offer" // ->المندوب قبل عرض التوصيل
    case storeAcceptOrder = "store_accept_order" // -> لما المتجر يقبل الطلب
}
