//
//  EmailNumberTextFieldView+Localization.swift
//  App
//
//  Created by MGAbouarab on 08/01/2024.
//

import Foundation

extension String {
    var emailLocalizable: String {
        return NSLocalizedString(self, tableName: "EmailLocalizable", bundle: Bundle.main, value: "", comment: "")
    }
}
