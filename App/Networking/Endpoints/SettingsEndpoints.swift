//
//  SettingsEndpoints.swift
//  App
//
//  Created by MGAbouarab on 22/12/2023.
//

import Foundation
import UIKit

struct SettingsEndpoints {
    private init() {}
}

extension SettingsEndpoints {
    static
    func intro() -> Endpoint<BaseResponse<[OnboardingModel]>> {
        return .init(
            path: "intros"
        )
    }
    
    static
    func getTerms() -> Endpoint<BaseResponse<String>>{
        return.init(path: "terms")
    }
    
    static
    func logOut() -> Endpoint<BaseResponse<EmptyResponse>>{
        return .init(method: .delete,
                     path: "sign-out",
                     body:
                        [
                            "deviceId": UserDefaults.pushNotificationToken ?? "no device id for firebase for this device and" ,
                            
                        ],
                     headerType: .authorized(token: UserDefaults.userData?.token ?? "")
        )
    }
    
    static
    func getGlobal() -> Endpoint<BaseResponse<GlobalDataModel>>{
        return.init(path: "global")
    }
    static
    func getCities(countryId:String) -> Endpoint<BaseResponse<[City]>>{
        return.init(path: "cities",
                    queries:
                        [
                            "countryId": countryId
                            
                        ]
        )
    }
    
    static
    func getCountries() -> Endpoint<BaseResponse<[Countries]>>{
        return.init(path: "countries")
    }
    
    
    static
    func deleteAccount() -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .delete,
                    path: "delete-account",
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")
        )
    }
    
    
    static
    func getProfile() -> Endpoint<BaseResponse<UserModel>>{
        return.init(path: "profile",
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")
        )
    }
    static
    func getWallet() -> Endpoint<BaseResponse<String>>{
        return.init(path: "wallet",
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")
        )
    }
    static
    func getAbout() -> Endpoint<BaseResponse<String>>{
        return.init(path: "about")
    }
    static
    func getPolicy() -> Endpoint<BaseResponse<String>>{
        return.init(path: "privacy")
    }
    
    static
    func getCommonQuestions() -> Endpoint<BaseResponse<[CommonQuestionsModel]>>{
        return.init(path: "fqs")
    }
    
    static
    func getPartners() -> Endpoint<BaseResponse<[PartnersModel]>>{
        return.init(path: "partners")
    }
    static
    func getPartnersDetails(partnerId: String) -> Endpoint<BaseResponse<PartnerDetailsModel>>{
        return.init(path: "partnerDetails",
                    queries:
                        [
                            "partnerId": partnerId
                        ]
                    
        )
    }
    static
    func contactus(phone: String, name: String, countryCode:String, title: String, message:String,type:String,centerId: String = "") -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method:.post,
                    path: "contactus",
                    body:
                        [
                            "phone":phone,
                            "name":name,
                            "userType":"patient",
                            "title":title,
                            "message":message,
                            "countryCode":countryCode,
                            "type": type,
                            "center": centerId
                        ]
        )
    }
    
    static
    func changeLanguage() -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .patch,
                    path: "changeLanguage",
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")
        )
    }
    static
    func updatePatientProfile(image:Data?, email: String, name: String ) -> Endpoint<BaseResponse<UserModel>>{
        var uploadData = [UploadData]()
        if let imageData = image {
            uploadData.append(.init(key: "avatar", data: imageData, mimeType: .jpg,fileName: "avatar.jpg"))
        }
        
        let body : [String:Any] =
        [
            "email":email,
            "name":name
        ]
        
        
        return.init(method: .patch,
                    path: "update-patient-profile",
                    body: body,
                    headerType: .authorized(token: UserDefaults.userData?.token ?? ""),
                    uploads: uploadData
        )
    }
    static
    func getMedicalRecords(type: String,page:Int) -> Endpoint<BaseResponse<[MedicalRecordsModel]>>{
        return.init(path: "medicalRecords",
                    queries:
                        [
                            "type":type,
                            "page":page
                        ],
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")
                    
        )
    }
    static
    func notificationSwitch(isNotify:Bool) -> Endpoint<BaseResponse<UserModel>>{
        return.init(method: .patch,
                    path: "isNotify",
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")
        )
    }
    static
    func getCompanions() -> Endpoint<BaseResponse<[CompanionsModel]>>{
        return.init(path: "companions",
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")

        )
    }
    static
    func addCompanionNewUser(name:String,phone:String,email:String,city:String,userType:String,countryCode:String,kinship:String) -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .post,
                    path: "add-companion-new-user",
                    body:
                    [
                        "name":name,
                        "phone":phone,
                        "email":email,
                        "city":city,
                        "userType":userType,
                        "countryCode":countryCode,
                        "kinship":kinship
                    ],
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")

        )
    }
    static
    func addCompanionExsitUser(phone: String,countryCode:String,kinship:String) -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .patch,
                    path: "add-companion-exist-user",
                    body:
                    [
                        "phone":phone,
                        "countryCode":countryCode,
                        "kinship":kinship
                    ],
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")

        )
    }
    static
    func deteteCompanion(userId: String) -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .delete, path:
                        "deleteCompanion",
                    body: 
                    [
                        "userId":userId
                    ],
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")

        )
    }
    static
    func getPaymentMethods() -> Endpoint<BaseResponse<PaymentMethod>>{
        return.init(path: "hyperPay-brands")
    }
    
    static
    func chargeWallet(price:String) -> Endpoint<BaseResponse<EmptyResponse>>{
        return.init(method: .patch,
                    path: "chargeWallet",
                    body:
                        [
                            "price":price
                        ],
                    headerType: .authorized(token: UserDefaults.userData?.token ?? "")
        )
    }
}
