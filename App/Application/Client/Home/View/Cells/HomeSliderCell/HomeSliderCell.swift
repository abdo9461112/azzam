//
//  HomeSliderCell.swift
//  App
//
//  Created by Abdo Emad on 15/07/2024.
//

import UIKit
import ImageSlideshow
import Kingfisher

class HomeSliderCell: UITableViewCell {

    private var datasource : HomeDataModel?
    @IBOutlet weak var imageSlider: ImageSlideshow!
    override func awakeFromNib() {
        super.awakeFromNib()
        configImageSlider()
        selectionStyle = .none
    }

    private func configImageSlider() {
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = .main
        pageControl.pageIndicatorTintColor = .white
        imageSlider.pageIndicator = pageControl
        imageSlider.slideshowInterval = 3
        imageSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        imageSlider.contentScaleMode = .scaleAspectFill
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(previewBanners))
        imageSlider.addGestureRecognizer(tapGesture)
    }
    
    @objc private func previewBanners() {
        if let link = datasource?.list[0].link, let url = URL(string: link), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    
    func configureCell(forRow row: HomeDataModel) {
        datasource = row
        var images = [KingfisherSource]()
        images.removeAll()
        for banner in row.list {
            if let banner = banner.image {
                images.append(KingfisherSource(urlString: banner)!)
            }
        }
        imageSlider.setImageInputs(images)
    }
    
}
