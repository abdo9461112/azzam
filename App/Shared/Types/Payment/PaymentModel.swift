//
//  PaymentModel.swift
//  App
//
//  Created by MGAbouarab on 09/01/2024.
//

import Foundation

struct PaymentModel: Decodable {
    
    let paymentMethods: [PaymentType]

    enum CodingKeys: String, CodingKey {
        case paymentMethods = "payment_methods"
    }
}
