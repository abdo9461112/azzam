//
//  CenterGroupsModel.swift
//  azzam-client
//
//  Created by Yosef elbosaty on 19/02/2024.
//

import Foundation

struct CenterGroupsModel: Codable {
    let consultationGroups: [CenterGroupsData]
    let showCenterFilter: Bool
}

struct CenterGroupsData: Codable {
    let _id, name, icon, color, slug: String
}
