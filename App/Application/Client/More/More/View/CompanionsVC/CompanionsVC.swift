//
//  CompanionsVC.swift
//  App
//
//  Created by Abdo Emad on 29/06/2024.
//

import UIKit

class CompanionsVC: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topStack: UIStackView!
    private var companionId: String = ""
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    private var companionsModel : [CompanionsModel] = [] {
        didSet{
            tableView.reloadData()
            if companionsModel.isEmpty {
                tableView.isHidden = true
                topStack.isHidden = false
            }else{
                topStack.isHidden = true
                tableView.isHidden = false
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        getCompanions()
    }
    private func setUp(){
        addBackButtonWith(title: "Companions".localized)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: CompanionsCell.self)
        
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        let vc = AddCompanionVC()
        push(vc)
    }
    
}
//MARK: - TableView
extension CompanionsVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        companionsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: CompanionsCell.self, for: indexPath)
        let item = companionsModel[indexPath.row]
        cell.setData(item)
        cell.onDelete = {[weak self] in
            self?.presentDeleteVc()
            self?.companionId = item.id
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        130
    }
    
}
//MARK: - NetWorking

extension CompanionsVC{
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int) -> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func getCompanions(){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.getCompanions()
            do{
                guard let response = try await request(endpoint)else{return}
                hideIndicator()
                companionsModel = response.data ?? []
            }catch{
                print("error trying to call get Companions api request : \(error.localizedDescription)")
            }
        }
    }
    private func deleteCompanion(id: String){
        Task{
            let endpoint = SettingsEndpoints.deteteCompanion(userId: id)
            do{
                guard let _ = try await request(endpoint)else{return}
                getCompanions()
            }catch{
                print("error trying to call delete Companions api request : \(error.localizedDescription)")
                
            }
        }
    }
}


//MARK: - presentDeleteVC

extension CompanionsVC: DeleteViewControllerDelegate{
    func presenSuccessVC() {
        let vc = SuccessVC.create(titleLable: "The companion has been deleted successfly".localized, titleButton: "Companion".localized)
        deleteCompanion(id: companionId)
        guard let sheet = vc.sheetPresentationController else { return }
        
        if #available(iOS 16.0, *) {
            let customDetent = UISheetPresentationController.Detent.custom { _ in
                return 275
            }
            sheet.detents = [customDetent]
            
        }
        
        sheet.preferredCornerRadius = 25
        vc.isModalInPresentation = true
        present(vc, animated: true)
        
    }
    private func presentDeleteVc() {
        let vc = DeleteVC.create(deletionType: .deleteCompanion, title: "Do you really want to delete the companion?".localized)
        vc.delegate = self 
        guard let sheet = vc.sheetPresentationController else { return }
        
        if #available(iOS 16.0, *) {
            let customDetent = UISheetPresentationController.Detent.custom { _ in
                return 290
            }
            sheet.detents = [customDetent]
            
        }
        
        sheet.preferredCornerRadius = 25
        vc.isModalInPresentation = true
        present(vc, animated: true)
    }
}



