//
//  MoreViewController.swift
//  App
//
//  Created by MGAbouarab on 16/01/2024.
//

import UIKit

class MoreViewController: BaseViewController {
    
    //MARK: - IBOutlets -
    @IBOutlet weak private var tableView: UITableView!
    
    //MARK: - Properties -
    private lazy var navigationUserInfoView: NavigationUserInfoView = NavigationUserInfoView()
    private var items: [MoreSection] = []
    var userModel : UserModel?
    private lazy var authItems: [MoreSection] = [
        .init(
            title: "Account_Information_Header_Title".localized,
            items: [
                .init(
                    image: .init(resource: .profile),
                    name: "Personal_Profile_More_Title".localized) { [weak self] in
                        let vc = ProfileVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .companions),
                    name: "Companions".localized) { [weak self] in
                        let vc = CompanionsVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .record),
                    name: "Medical records".localized) { [weak self] in
                        let vc = MedicalRecordsVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .wallet),
                    name: "Wallet_More_Title".localized) { [weak self] in
                        let vc = WalletVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    }
            ]
        ),
        .init(
            title: "General_Settings_Header_Title".localized,
            items: [
                .init(
                    image: .init(resource: .settings),
                    name: "Settings".localized) { [weak self] in
                        let vc = SettingsVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
            ]
        ),
        .init(
            title: "General_Information_Header_Title".localized,
            items: [
                .init(
                    image: .init(resource: .aboutUs),
                    name: "About_Us_More_Title".localized) { [weak self] in
                        let vc = TermsAndAboutAndPolicyVC.create(type: .about)
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .faqs),
                    name: "Common questions".localized) { [weak self] in
                        let vc = CommonQuestionsVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .partners),
                    name: "Partners".localized) { [weak self] in
                        let vc = PartnersVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .contactUs),
                    name: "Contact_Us_More_Title".localized) { [weak self] in
                        let vc = ConnectUsVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .policy),
                    name: "Policy".localized) { [weak self] in
                        let vc = TermsAndAboutAndPolicyVC.create(type: .policy)
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .terms),
                    name: "Terms and condtions".localized,accessoryType: .none) { [weak self] in
                        let vc = TermsAndAboutAndPolicyVC.create(type: .terms)
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    }
            ]
        ),
        .init(
            title: "", items:
                [
                    .init(
                        image: .init(resource: .reportFlag),
                        name: "Delete account".localized,
                        accessoryType: .none,
                        backgroundColor: .lightPink)
                    { [weak self ] in
                        self?.presentDeleteVc()
                    }
                ]
        ),
        .init(title: "", items:
                [
                    .init(
                        image: .init(resource: .exit),
                        name: "Log out".localized,
                        accessoryType: .none,
                        backgroundColor: .lightPink)
                    { [weak self ] in
                        self?.logout()
                    }
                ]
             )
    ]
    private lazy var visitorItems: [MoreSection] = [
        .init(
            title: "General_Information_Header_Title".localized,
            items: [
                .init(
                    image: .init(resource: .aboutUs),
                    name: "About_Us_More_Title".localized) { [weak self] in
                        let vc = TermsAndAboutAndPolicyVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .faqs),
                    name: "Common questions".localized) { [weak self] in
                        let vc = CommonQuestionsVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .partners),
                    name: "Partners".localized) { [weak self] in
                        let vc = PartnersVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .contactUs),
                    name: "Contact_Us_More_Title".localized) { [weak self] in
                        let vc = ConnectUsVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .policy),
                    name: "Policy".localized) { [weak self] in
                        let vc = TermsAndAboutAndPolicyVC()
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    },
                .init(
                    image: .init(resource: .terms),
                    name: "Terms and condtions".localized,accessoryType: .none) { [weak self] in
                        let vc = TermsAndAboutAndPolicyVC.create(type: .terms)
                        vc.hidesBottomBarWhenPushed = true
                        self?.push(vc)
                    }
            ]
        ),
        .init(title: "", items:
                [
                    .init(
                        image: .init(resource: .exit),
                        name: UserDefaults.isLogin ? "Log out".localized : "Login".localized,
                        accessoryType: .none,
                        backgroundColor: .lightPink)
                    { [weak self ] in
                        self?.logout()
                    }
                ]
             )
    ]
    
    private let responseHandler : ResponseHandler = DefaultResponseHandler()
    //MARK: - Lifecycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationDesign()
        self.setupTableView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.items = !UserDefaults.isLogin ? visitorItems : authItems
        self.navigationUserInfoView.set(
            status: UserDefaults.isLogin ? .authorized(
                name: UserDefaults.userData?.name,
                image: UserDefaults.userData?.avatar,
                rate: "")
            : .unauthorized
        )
        (self.navigationController as? ColoredNav)?.changeApperance(to: .light)
        self.tableView.reloadData()
    }
    private func setupNavigationDesign() {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.leftBarButtonItem = .init(customView: self.navigationUserInfoView)
    }
    
    private func presentDeleteVc() {
        let vc = DeleteVC.create(deletionType: .deleteAccount, title: "Do you really want to delete this account?".localized)
        guard let sheet = vc.sheetPresentationController else { return }
        
        if #available(iOS 16.0, *) {
            let customDetent = UISheetPresentationController.Detent.custom { _ in
                return 290
            }
            sheet.detents = [customDetent]
            
        }
        
        sheet.preferredCornerRadius = 25
        vc.isModalInPresentation = true
        present(vc, animated: true)
    }
}

//MARK: - UITableView -
private extension MoreViewController {
    func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView.contentInset = .init(top: 20, left: 0, bottom: 20, right: 0)
    }
}

extension MoreViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items[section].items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.items[indexPath.section].items[indexPath.row]
        
        let cell = UITableViewCell()
        cell.contentConfiguration = UIListContentConfiguration.defaultSubtitleCell(
            image: item.image,
            title: item.name
        )
        cell.accessoryType = item.accessoryType
        cell.backgroundColor = item.backgroundColor
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let _ = self.items[section].title else {return 0}
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let title = self.items[section].title else {return nil}
        return SectionHeaderView(title: title)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.items[indexPath.section].items[indexPath.row].action()
    }
    
}

//MARK: -  Networking

extension MoreViewController {
    private func request<ResponseData: Decodable>(_ endPoint: Endpoint<ResponseData>, progress: ((_ progress: Int)-> Void)? = nil) async throws -> ResponseData? {
        return try await self.responseHandler.get(endPoint, progress: progress)
    }
    private func logout(){
        Task{
            showIndicator()
            let endpoint = SettingsEndpoints.logOut()
            do{
                guard let _ = try await request(endpoint)else{return}
                hideIndicator()
                UserDefaults.userData = nil
                UserDefaults.isFirstTime = true
                UserDefaults.isLogin = false
                let vc = LoginVC()
                let nav = BaseNavigationController(rootViewController: vc)
                AppHelper.changeWindowRoot(vc: nav)
            }catch{
                print("error trying to call logout api request : \(error.localizedDescription)")
            }
        }
    }
}

