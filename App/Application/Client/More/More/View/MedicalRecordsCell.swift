//
//  MedicalRecordsCell.swift
//  App
//
//  Created by Abdo Emad on 07/07/2024.
//

import UIKit

class MedicalRecordsCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var startingDate: UILabel!
    @IBOutlet weak var endingDate: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        if Language.isRTL(){
            arrowImage.image = UIImage(systemName: "arrowshape.right.fill")
        }else{
            arrowImage.image = UIImage(systemName: "arrowshape.left.fill")
        }
    }
    
    func setData(_ data : MedicalRecordsModel){
        startingDate.text = data.startedAt
        endingDate.text = data.endedAt
    }

}
